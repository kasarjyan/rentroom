from PyQt5.QtCore import QObject, QStateMachine, QState, QFinalState, QEvent

from api_client import _client, init_client
from apartments.models import User
from states import SigninState, AdminState, ClientState, RealtorState
from views import LoginDialog, MainWindow, Views

class Controller(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        init_client()
        self.init_views()
        self.createStateMachine()
        self.createTransitions()

    def init_views(self):
        Views.loginDialog = LoginDialog()
        Views.mainWindow= MainWindow()
        
    def createStateMachine(self):
        self.startState = QState()
        self.finalState = QFinalState()

        self.signinState = SigninState(self.startState)
        self.adminState = AdminState(self.startState)
        self.realtorState = RealtorState(self.startState)
        self.clientState = ClientState(self.startState)
        
        self.stateMachine = QStateMachine()

        self.startState.setInitialState(self.signinState)

        self.stateMachine.addState(self.startState);
        self.stateMachine.addState(self.finalState);
        self.stateMachine.setInitialState(self.startState);

    def createTransitions(self):
        self.signinState.addTransition(self.signinState.admin_signed, self.adminState)
        self.signinState.addTransition(self.signinState.realtor_signed, self.realtorState)
        self.signinState.addTransition(self.signinState.client_signed, self.clientState)

        #Sign out
        self.adminState.addTransition(self.adminState.signout_requested, self.signinState)        
        self.realtorState.addTransition(self.realtorState.signout_requested, self.signinState)        
        self.clientState.addTransition(self.clientState.signout_requested, self.signinState)
        


    def start(self):
    	self.stateMachine.start();