import sys
import os
from PyQt5.QtWidgets import QApplication
from controller import Controller
import resources.resources

if __name__ == "__main__":
    sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../apartments/apartments'))

    app = QApplication(sys.argv)

    app.setApplicationName("Apartments Client")
    app.setApplicationDisplayName("Apartments Client")
    app.setApplicationVersion("1.0")

    controller = Controller()
    controller.start()
    sys.exit(app.exec_())

