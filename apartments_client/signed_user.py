from apartments.models import User

class SignedUser:
    def __init__(self):
        self._user = None
        self._token = None
    @property
    def user(self) -> User:
        return self._user
    @user.setter
    def user(self, user:User):
        self._user = user
    @property
    def token(self)->str:
        return self._token
    @token.setter
    def token(self, token:str):
        self._token = token
    @property
    def userid(self)->int:
        return self._user.id

signedUser = SignedUser()