from PyQt5.QtCore import QObject, QMetaObject, pyqtSignal, pyqtSlot
from apartments.models.user import User
from apartments.encoder import CustomJSONEncoder

import json
import requests
import requests


HOST = "http://localhost"
PORT = 5555

def getURL(endpoint):
    return "{}:{}/{}".format(HOST, PORT, endpoint)

class ResponseData:
    def __init__(self):
        self._status = False
        self._code = 500
        self._json = None

    @property
    def status(self)->bool:
        return self._status
    @status.setter
    def status(self, status: bool):
        self._status = status
        
    @property
    def json(self):
        return self._json
    @json.setter
    def json(self, json):
        self._json = json
        
    @property
    def code(self)->int:
        return self._code
    @code.setter
    def code(self, code: int):
        self._code = code

class Client(QObject):
    """API Client class"""
    login_ready = pyqtSignal('PyQt_PyObject')
    signup_ready = pyqtSignal('PyQt_PyObject')
    get_user_ready = pyqtSignal('PyQt_PyObject')
    get_apartments_ready = pyqtSignal('PyQt_PyObject')
    get_clients_ready = pyqtSignal('PyQt_PyObject')
    get_realtors_ready = pyqtSignal('PyQt_PyObject')
    delete_apartment_ready = pyqtSignal('PyQt_PyObject')
    update_apartment_ready = pyqtSignal('PyQt_PyObject')
    delete_client_ready = pyqtSignal('PyQt_PyObject')
    update_client_ready = pyqtSignal('PyQt_PyObject')
    delete_realtor_ready = pyqtSignal('PyQt_PyObject')
    update_realtor_ready = pyqtSignal('PyQt_PyObject')
    rent_ready = pyqtSignal('PyQt_PyObject')
    cancel_rent_ready = pyqtSignal('PyQt_PyObject')

    add_apartment_ready = pyqtSignal('PyQt_PyObject')
    add_client_ready = pyqtSignal('PyQt_PyObject')
    add_realtor_ready = pyqtSignal('PyQt_PyObject')


    def __init__(self, parent=None):
        super(Client,self).__init__(parent)
    
    @pyqtSlot('PyQt_PyObject')
    def login(self, user: User):
        responseData = ResponseData()
        payload = json.dumps(user, cls=CustomJSONEncoder)
        headers = {
                    'Content-Type': "application/json"
                  }
        print(getURL('login'))
        
        response = None
        try:
            response = requests.request("POST", getURL('login'), data=payload, headers=headers)
            responseData.status = response.status_code == 200
            responseData.json = response.json()
            responseData.code = response.status_code            
        except:
            responseData.json = {"msg":"Exception user is requested {}".format(response)}
        self.login_ready.emit(responseData)

    @pyqtSlot('PyQt_PyObject')
    def signup(self, user: User)->ResponseData:
        responseData = ResponseData()
        path='admins'
        if user.role == 2:
            path = "realtors"
        elif user.role == 3:
            path = "clients"

        url = getURL("{}".format(path))
        payload = json.loads(json.dumps(user, cls=CustomJSONEncoder))
        del payload['role']
        payload = json.dumps(payload)
        print("Payload={}".format(payload))
        headers = {
                    'Content-Type': "application/json"
                  }        
        response = None
        try:
            response = requests.request("POST", url, data=payload, headers=headers)
            responseData.status = response.status_code == 201
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception user is requested {}".format(response)}
        self.signup_ready.emit(responseData)

    @pyqtSlot(int, int, str)
    def get_user(self, userid, role, token):
        responseData = ResponseData()
        path='admins'
        if role == 2:
            path = "realtors"
        elif role == 3:
            path = "clients"

        url = getURL("{}/{}".format(path, userid))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("GET", url, headers=headers)

            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception user is requested {}".format(response)}

        self.get_user_ready.emit(responseData)

    @pyqtSlot(str)
    def get_apartments(self, token):
        responseData = ResponseData()

        url = getURL("apartments")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("GET", url, data='{}', headers=headers)

            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception apartments is requested {}".format(response)}

        self.get_apartments_ready.emit(responseData)


    @pyqtSlot(str)
    def get_clients(self, token):
        responseData = ResponseData()

        url = getURL("clients")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("GET", url, data='{}', headers=headers)

            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception clients is requested {}".format(response)}

        self.get_clients_ready.emit(responseData)

    @pyqtSlot(str)
    def get_realtors(self, token):
        responseData = ResponseData()

        url = getURL("realtors")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("GET", url, data='{}', headers=headers)

            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception realtors is requested {}".format(response)}

        self.get_realtors_ready.emit(responseData)
    
    @pyqtSlot(str, int)
    def delete_apartment(self, token, id):
        responseData = ResponseData()

        url = getURL("apartments/{}".format(id))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("DELETE", url, data='{}', headers=headers)
            responseData.status = response.status_code == 204
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception delete apartment is requested {}".format(response)}
        self.delete_apartment_ready.emit(responseData)

    @pyqtSlot(str, int, str)
    def update_apartment(self, token, id, jsonStr):
        responseData = ResponseData()

        url = getURL("apartments/{}".format(id))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("PUT", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception update apartment is requested {}".format(response)}
        self.update_apartment_ready.emit(responseData)

    @pyqtSlot(str, int)
    def delete_client(self, token, id):
        responseData = ResponseData()

        url = getURL("clients/{}".format(id))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("DELETE", url, data='{}', headers=headers)
            responseData.status = response.status_code == 204
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception delete client is requested {}".format(response)}
        self.delete_client_ready.emit(responseData)

    @pyqtSlot(str, int, str)
    def update_client(self, token, id, jsonStr):
        responseData = ResponseData()

        url = getURL("clients/{}".format(id))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("PUT", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception update client is requested {}".format(response)}
        self.update_client_ready.emit(responseData)

    @pyqtSlot(str, int)
    def delete_realtor(self, token, id):
        responseData = ResponseData()

        url = getURL("realtors/{}".format(id))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("DELETE", url, data='{}', headers=headers)
            responseData.status = response.status_code == 204
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception delete client is requested {}".format(response)}
        self.delete_realtor_ready.emit(responseData)

    @pyqtSlot(str, int, str)
    def update_realtor(self, token, id, jsonStr):
        responseData = ResponseData()

        url = getURL("realtors/{}".format(id))
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("PUT", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception update realtor is requested {}".format(response)}
        self.update_realtor_ready.emit(responseData)

    @pyqtSlot(str, str)
    def rent(self, token, jsonStr):
        responseData = ResponseData()
        url = getURL("rentals")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("POST", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 201
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception rent is requested {}".format(response)}
        self.rent_ready.emit(responseData)

    @pyqtSlot(str, str)
    def cancel_rent(self, token, jsonStr):
        responseData = ResponseData()
        url = getURL("rentals")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("DELETE", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 200
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception cancel rent is requested {}".format(response)}
        self.rent_ready.emit(responseData)


    @pyqtSlot(str, str)
    def add_client(self, token, jsonStr):
        responseData = ResponseData()
        url = getURL("clients")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("POST", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 201
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception create client is requested {}".format(response)}
        self.add_client_ready.emit(responseData)

    @pyqtSlot(str, str)
    def add_realtor(self, token, jsonStr):
        responseData = ResponseData()
        url = getURL("realtors")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("POST", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 201
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception create realtor is requested {}".format(response)}
        self.add_realtor_ready.emit(responseData)

    @pyqtSlot(str, str)
    def add_apartment(self, token, jsonStr):
        responseData = ResponseData()
        url = getURL("apartments")
        headers = {
                    'Content-Type': "application/json",
                    "Authorization": "Bearer {}".format(token)
                  }
        response = None
        try:
            response = requests.request("POST", url, data=jsonStr, headers=headers)
            responseData.status = response.status_code == 201
            responseData.json = json.loads(response.text)
            responseData.code = response.status_code
        except:
            responseData.json = {"msg":"Exception create apartment is requested {}".format(response)}
        self.add_apartment_ready.emit(responseData)
