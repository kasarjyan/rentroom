from PyQt5.QtCore import QThread
from api_client.client import Client, ResponseData
_client = Client()
worker_thread = QThread()

def init_client():
    _client.moveToThread(worker_thread)
    worker_thread.start()
