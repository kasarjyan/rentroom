from PyQt5.QtCore import QState, pyqtSignal, pyqtSlot, Q_ARG, QMetaObject, Qt
from PyQt5.QtWidgets import QApplication
from views import Views
from api_client import _client, ResponseData
from apartments.models import User

class SigninState(QState):
    admin_signed = pyqtSignal()
    client_signed = pyqtSignal()
    realtor_signed = pyqtSignal()
    def __init__(self, parent=None):
        super().__init__(parent)
        
    def onEntry(self, event):
        super().onEntry(event)
        self.createConnections()
        Views.loginDialog.show()
        print("SignIn state on entry")

    def onExit(self, event):
        super().onExit(event)
        self.disconnectConnections()
        Views.loginDialog.hide()
        print("Signin state on exit")
    
    def createConnections(self):
        Views.loginDialog.signin_requested.connect(self.on_signin_requested)
        Views.loginDialog.signup_requested.connect(self.on_signup_requested)
        Views.loginDialog.user_requested.connect(self.on_user_requested)
        Views.loginDialog.signin_success.connect(self.on_signin_success)
        _client.login_ready.connect(self.on_login_ready)
        _client.get_user_ready.connect(self.on_get_user_ready)
        _client.signup_ready.connect(self.on_signup_ready)

    def disconnectConnections(self):
        Views.loginDialog.signin_requested.disconnect(self.on_signin_requested)
        Views.loginDialog.signup_requested.disconnect(self.on_signup_requested)
        Views.loginDialog.user_requested.disconnect(self.on_user_requested)
        Views.loginDialog.signin_success.disconnect(self.on_signin_success)
        _client.login_ready.disconnect(self.on_login_ready)
        _client.get_user_ready.disconnect(self.on_get_user_ready)
        _client.signup_ready.disconnect(self.on_signup_ready)
        


    @pyqtSlot('PyQt_PyObject')
    def on_signin_requested(self, user):      
        QApplication.setOverrideCursor(Qt.WaitCursor);  
        QMetaObject.invokeMethod(_client,'login', Q_ARG('PyQt_PyObject', user))

    @pyqtSlot('PyQt_PyObject')
    def on_signup_requested(self, user):   
        QApplication.setOverrideCursor(Qt.WaitCursor);     
        QMetaObject.invokeMethod(_client,'signup', Q_ARG('PyQt_PyObject', user))

    @pyqtSlot('PyQt_PyObject')
    def on_login_ready(self, responseData):
        Views.loginDialog.on_login_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_signup_ready(self, responseData):
        Views.loginDialog.on_signup_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot(int, int, str)
    def on_user_requested(self, id, role, token):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client, 'get_user', Q_ARG(int, id), Q_ARG(int, role), Q_ARG(str, token))

    @pyqtSlot('PyQt_PyObject')
    def on_get_user_ready(self, responseData):
        Views.loginDialog.on_user_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_signin_success(self,user:User):
        QApplication.restoreOverrideCursor()
        if user.role == 1:
            self.admin_signed.emit()
        elif user.role == 2:
            self.realtor_signed.emit()
        elif user.role == 3:
            self.client_signed.emit()
