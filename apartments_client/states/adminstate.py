from PyQt5.QtCore import QState, pyqtSignal, pyqtSlot, Q_ARG, QMetaObject, Qt
from PyQt5.QtWidgets import QApplication
from views import Views
from api_client import _client, ResponseData
from signed_user import signedUser


from apartments.models import User

class AdminState(QState):
    signout_requested = pyqtSignal()
    def __init__(self, parent=None):
        super().__init__(parent)

    def onEntry(self, event):
        super().onEntry(event)
        self.createConnections()
        Views.mainWindow.setAdminCurrentView()
        Views.mainWindow.showMaximized()
        Views.mainWindow._adminView.on_apartments_selected()
        self.on_realtors_requested()
        print("Admin state on entry")
        
    def onExit(self, event):
        super().onExit(event)
        self.disconnectConnections()
        Views.mainWindow.hide()
        print("Admin state on exit")

    def createConnections(self):
        Views.mainWindow._adminView.signout_clicked.connect(self.signout_requested)

        Views.mainWindow._adminView.apartments_requested.connect(self.on_apartments_requested)
        Views.mainWindow._adminView.realtors_requested.connect(self.on_realtors_requested)
        Views.mainWindow._adminView.clients_requested.connect(self.on_clients_requested)

        Views.mainWindow._adminView.delete_apartment_requested.connect(self.on_delete_apartment_requested)
        Views.mainWindow._adminView.update_apartment_requested.connect(self.on_update_apartment_requested)
        Views.mainWindow._adminView.delete_client_requested.connect(self.on_delete_client_requested)
        Views.mainWindow._adminView.update_client_requested.connect(self.on_update_client_requested)
        Views.mainWindow._adminView.delete_realtor_requested.connect(self.on_delete_realtor_requested)
        Views.mainWindow._adminView.update_realtor_requested.connect(self.on_update_realtor_requested)


        
        Views.mainWindow._adminView.add_client_requested.connect(self.on_add_client_requested)
        Views.mainWindow._adminView.add_realtor_requested.connect(self.on_add_realtor_requested)
        Views.mainWindow._adminView.add_apartment_requested.connect(self.on_add_apartment_requested)

        _client.get_apartments_ready.connect(self.on_get_apartments_ready)
        _client.get_clients_ready.connect(self.on_get_clients_ready)
        _client.get_realtors_ready.connect(self.on_get_realtors_ready)

        _client.update_apartment_ready.connect(self.on_update_apartment_ready)
        _client.delete_apartment_ready.connect(self.on_delete_apartment_ready)
        _client.update_client_ready.connect(self.on_update_client_ready)
        _client.delete_client_ready.connect(self.on_delete_client_ready)
        _client.update_realtor_ready.connect(self.on_update_realtor_ready)
        _client.delete_realtor_ready.connect(self.on_delete_realtor_ready)

        _client.add_client_ready.connect(self.on_add_client_ready)
        _client.add_realtor_ready.connect(self.on_add_realtor_ready)
        _client.add_apartment_ready.connect(self.on_add_apartment_ready)


    def disconnectConnections(self):
        Views.mainWindow._adminView.signout_clicked.disconnect(self.signout_requested)
        Views.mainWindow._adminView.apartments_requested.disconnect(self.on_apartments_requested)
        Views.mainWindow._adminView.realtors_requested.disconnect(self.on_realtors_requested)
        Views.mainWindow._adminView.clients_requested.disconnect(self.on_clients_requested)

        Views.mainWindow._adminView.delete_apartment_requested.disconnect(self.on_delete_apartment_requested)
        Views.mainWindow._adminView.update_apartment_requested.disconnect(self.on_update_apartment_requested)
        Views.mainWindow._adminView.delete_client_requested.disconnect(self.on_delete_client_requested)
        Views.mainWindow._adminView.update_client_requested.disconnect(self.on_update_client_requested)
        Views.mainWindow._adminView.delete_realtor_requested.disconnect(self.on_delete_realtor_requested)
        Views.mainWindow._adminView.update_realtor_requested.disconnect(self.on_update_realtor_requested)
        
        Views.mainWindow._adminView.add_client_requested.disconnect(self.on_add_client_requested)
        Views.mainWindow._adminView.add_realtor_requested.disconnect(self.on_add_realtor_requested)
        Views.mainWindow._adminView.add_apartment_requested.disconnect(self.on_add_apartment_requested)

        _client.get_apartments_ready.disconnect(self.on_get_apartments_ready)
        _client.get_clients_ready.disconnect(self.on_get_clients_ready)
        _client.get_realtors_ready.disconnect(self.on_get_realtors_ready)
        _client.update_apartment_ready.disconnect(self.on_update_apartment_ready)
        _client.delete_apartment_ready.disconnect(self.on_delete_apartment_ready)
        _client.update_client_ready.disconnect(self.on_update_client_ready)
        _client.delete_client_ready.disconnect(self.on_delete_client_ready)
        _client.update_realtor_ready.disconnect(self.on_update_realtor_ready)
        _client.delete_realtor_ready.disconnect(self.on_delete_realtor_ready)
        
        _client.add_client_ready.disconnect(self.on_add_client_ready)
        _client.add_realtor_ready.disconnect(self.on_add_realtor_ready)
        _client.add_apartment_ready.disconnect(self.on_add_apartment_ready)

    def on_apartments_requested(self):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'get_apartments', Q_ARG(str, signedUser.token))

    def on_realtors_requested(self):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'get_realtors', Q_ARG(str, signedUser.token))

    def on_clients_requested(self):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'get_clients', Q_ARG(str, signedUser.token))



    def on_delete_apartment_requested(self, id):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'delete_apartment', Q_ARG(str, signedUser.token), Q_ARG(int, id))
    
    def on_update_apartment_requested(self, id, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'update_apartment', Q_ARG(str, signedUser.token), Q_ARG(int, id), Q_ARG(str, jsonStr))

    def on_delete_client_requested(self, id):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'delete_client', Q_ARG(str, signedUser.token), Q_ARG(int, id))
    
    def on_update_client_requested(self, id, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'update_client', Q_ARG(str, signedUser.token), Q_ARG(int, id), Q_ARG(str, jsonStr))

    def on_delete_realtor_requested(self, id):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'delete_realtor', Q_ARG(str, signedUser.token), Q_ARG(int, id))
    
    def on_update_realtor_requested(self, id, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'update_realtor', Q_ARG(str, signedUser.token), Q_ARG(int, id), Q_ARG(str, jsonStr))

    def on_add_client_requested(self, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'add_client', Q_ARG(str, signedUser.token), Q_ARG(str, jsonStr))

    def on_add_realtor_requested(self, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'add_realtor', Q_ARG(str, signedUser.token), Q_ARG(str, jsonStr))

    def on_add_apartment_requested(self, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'add_apartment', Q_ARG(str, signedUser.token), Q_ARG(str, jsonStr))


    @pyqtSlot('PyQt_PyObject')
    def on_get_apartments_ready(self, responseData):
        Views.mainWindow._adminView.on_get_apartments_ready(responseData)
        QApplication.restoreOverrideCursor()
        
    @pyqtSlot('PyQt_PyObject')
    def on_get_realtors_ready(self, responseData):
        Views.mainWindow._adminView.on_get_realtors_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_get_clients_ready(self, responseData):
        Views.mainWindow._adminView.on_get_clients_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_update_apartment_ready(self, responseData):
        Views.mainWindow._adminView.on_update_apartment_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_delete_apartment_ready(self, responseData):
        Views.mainWindow._adminView.on_delete_apartment_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_update_client_ready(self, responseData):
        Views.mainWindow._adminView.on_update_client_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_delete_client_ready(self, responseData):
        Views.mainWindow._adminView.on_delete_client_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_update_realtor_ready(self, responseData):
        Views.mainWindow._adminView.on_update_realtor_ready(responseData)

    @pyqtSlot('PyQt_PyObject')
    def on_delete_realtor_ready(self, responseData):
        Views.mainWindow._adminView.on_delete_realtor_ready(responseData)
        QApplication.restoreOverrideCursor()


    @pyqtSlot('PyQt_PyObject')
    def on_add_client_ready(self, responseData):
        Views.mainWindow._adminView.on_add_client_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_add_realtor_ready(self, responseData):
        Views.mainWindow._adminView.on_add_realtor_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_add_apartment_ready(self, responseData):
        Views.mainWindow._adminView.on_add_apartment_ready(responseData)
        QApplication.restoreOverrideCursor()