from PyQt5.QtCore import QState, pyqtSignal, pyqtSlot, Q_ARG, QMetaObject, Qt
from PyQt5.QtWidgets import QApplication
from views import Views
from api_client import _client, ResponseData
from signed_user import signedUser
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication

class ClientState(QState):
    signout_requested = pyqtSignal()
    def __init__(self, parent=None):
        super().__init__(parent)   

    def onEntry(self, event):
        super().onEntry(event)
        self.createConnections()
        Views.mainWindow.setClientCurrentView()
        self.on_apartments_requested()
        Views.mainWindow.showMaximized()
        print("Client state on entry")

        
    def onExit(self, event):
        super().onExit(event)
        self.disconnectConnections()
        Views.mainWindow.hide()
        print("Client state on exit")

    def createConnections(self):
        Views.mainWindow._clientView.signout_clicked.connect(self.signout_requested)

        Views.mainWindow._clientView.apartments_requested.connect(self.on_apartments_requested)

        Views.mainWindow._clientView.rent_requested.connect(self.on_rent_requested)
        Views.mainWindow._clientView.cancel_rent_requested.connect(self.on_cancel_rent_requested)

        #Views.mainWindow._clientView.update_client_requested.connect(self.on_update_client_requested)

        _client.get_apartments_ready.connect(self.on_get_apartments_ready)

        _client.update_client_ready.connect(self.on_update_client_ready)
        _client.delete_client_ready.connect(self.on_delete_client_ready)
        
        _client.rent_ready.connect(self.on_rent_ready)
        _client.cancel_rent_ready.connect(self.on_cancel_rent_ready)


    def disconnectConnections(self):
        Views.mainWindow._clientView.signout_clicked.disconnect(self.signout_requested)
        Views.mainWindow._clientView.apartments_requested.disconnect(self.on_apartments_requested)

        
        Views.mainWindow._clientView.rent_requested.disconnect(self.on_rent_requested)
        Views.mainWindow._clientView.cancel_rent_requested.disconnect(self.on_cancel_rent_requested)

        #Views.mainWindow._clientView.update_apartment_requested.disconnect(self.on_update_apartment_requested)
        #Views.mainWindow._clientView.delete_client_requested.disconnect(self.on_delete_client_requested)
        #Views.mainWindow._clientView.update_client_requested.disconnect(self.on_update_client_requested)
        
        _client.get_apartments_ready.disconnect(self.on_get_apartments_ready)

        _client.update_client_ready.disconnect(self.on_update_client_ready)
        _client.delete_client_ready.disconnect(self.on_delete_client_ready)

        _client.rent_ready.connect(self.on_rent_ready)
        _client.cancel_rent_ready.connect(self.on_cancel_rent_ready)

        
    def on_apartments_requested(self):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'get_apartments', Q_ARG(str, signedUser.token))

    def on_update_apartment_requested(self, id, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'update_apartment', Q_ARG(str, signedUser.token), Q_ARG(int, id), Q_ARG(str, jsonStr))

    def on_delete_client_requested(self, id):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'delete_client', Q_ARG(str, signedUser.token), Q_ARG(int, id))
    
    def on_update_client_requested(self, id, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'update_client', Q_ARG(str, signedUser.token), Q_ARG(int, id), Q_ARG(str, jsonStr))

  
    def on_rent_requested(self, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'rent', Q_ARG(str, signedUser.token), Q_ARG(str, jsonStr))
    
    def on_cancel_rent_requested(self, jsonStr):
        QApplication.setOverrideCursor(Qt.WaitCursor);
        QMetaObject.invokeMethod(_client,'cancel_rent', Q_ARG(str, signedUser.token), Q_ARG(str, jsonStr))


    @pyqtSlot('PyQt_PyObject')
    def on_get_apartments_ready(self, responseData):
        Views.mainWindow._clientView.on_get_apartments_ready(responseData)
        QApplication.restoreOverrideCursor()
 
    @pyqtSlot('PyQt_PyObject')
    def on_update_apartment_ready(self, responseData):
        Views.mainWindow._clientView.on_update_apartment_ready(responseData)
        QApplication.restoreOverrideCursor()

 
    @pyqtSlot('PyQt_PyObject')
    def on_update_client_ready(self, responseData):
        Views.mainWindow._clientView.on_update_client_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_delete_client_ready(self, responseData):
        Views.mainWindow._clientView.on_delete_client_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_rent_ready(self, responseData):
        Views.mainWindow._clientView.on_rent_ready(responseData)
        QApplication.restoreOverrideCursor()

    @pyqtSlot('PyQt_PyObject')
    def on_cancel_rent_ready(self, responseData):
        Views.mainWindow._clientView.on_cancel_rent_ready(responseData)
        QApplication.restoreOverrideCursor()
