from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

class MapsBridge(QObject):
    markerClicked = pyqtSignal(int, float, float)
    locationPicked = pyqtSignal(float, float)

    def __init__(self):
        super().__init__()

    @pyqtSlot(int, str, str) 
    def onMarkerClicked(self, id, latitude, longitude):
        print("Python -> Marker Clicked: {} {} {}".format(id,latitude, longitude))
        self.markerClicked.emit(int(id), float(latitude), float(longitude))
    @pyqtSlot(str, str)
    def onLocationPick(self, latitude, longitude):
        print("Python-> Marker Clicked: {} {}".format(latitude, longitude))
        self.locationPicked.emit(float(latitude), float(longitude))

