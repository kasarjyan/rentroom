from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtCore import *
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWebChannel import QWebChannel
from views.maps.mapsbridge import MapsBridge
import resources.resources
from apartments.models import Apartment

def readFile(filePath):
    f = QFile(filePath)
    if f.open(QIODevice.ReadOnly):
        res = f.readAll()
        res = str(res.data(), encoding='utf-8')
        return res
    return ''



class MapsView(QWidget):
    markerClicked = pyqtSignal(int)
    loadFinished  = pyqtSignal(bool)
    locationPicked = pyqtSignal(float, float)
    def __init__(self):
        super().__init__()
        self._webView = QWebEngineView()
        self._webChannel = QWebChannel()
        self._mapsBridge = MapsBridge()
        self.setupChannel()
        self.setupLayout()
        self.setupConnections()

    def setupChannel(self):
        html = readFile(":/Resources/map.html");
        self._webView.setHtml(html, QUrl("qrc:/Resources/"))
        self._webChannel.registerObject("mapsBridge", self._mapsBridge)
        self._webView.page().setWebChannel(self._webChannel)

    def setupLayout(self):
        hlay = QHBoxLayout();
        hlay.addWidget(self._webView);
        hlay.setContentsMargins(0, 0, 5, 0);
        self.setLayout(hlay);

    def setupConnections(self):
        self._mapsBridge.markerClicked.connect(self.onMarkerClicked)
        self._mapsBridge.locationPicked.connect(self.onLocationPick)
        self._webView.loadFinished.connect(self.loadFinished)

    def onMarkerClicked(self, id, latitude, longitude):
        print("Python signal/slot onMarkerClicked testing {} {} {}".format(id, latitude, longitude))
        self.markerClicked.emit(id)
        
    def onLocationPick(self, latitude, longitude):
        print("Python signal/slot onLocationPick testing {} {}".format(latitude, longitude))
        self.locationPicked.emit(latitude, longitude)

    def setApartments(self, apartments):
        self.removeMarkers();
        for apartment in apartments:
            rented = 1
            if apartment.status == 'available':
                rented = 0
            cmd='addMarker({}, {}, "{}", {}, {});'.format(apartment.latitude, apartment.longitude, apartment.name, apartment.id, rented)
            print(cmd)
            self._webView.page().runJavaScript(cmd)
            
    def removeMarker(self, apartment):
        self._webView.page().runJavaScript("removeMarker({});".format(apartment.id))
    
    def removeMarkers(self):
        self._webView.page().runJavaScript("removeMarkers();")

    def panToApartment(self, apartment):        
        self._webView.page().runJavaScript("panToApartment({});".format(apartment.id))
