from views.widget import Widget, ClickableLabel
from apartments.models import User
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QSize, QEvent
class SideBar(Widget):
    signout_clicked = pyqtSignal()
    current_changed = pyqtSignal(int)
    add_new_requested = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

    def create_widgets(self):
        self.signoutBtn = QPushButton("Sign out")
        self.listView = QListView()
        self.roleLabel = QLabel("Role")
        self.nameLabel = QLabel("Name")
        self.addNewLabel = ClickableLabel(self.listView)
        self.addNewLabel.setPixmap(QPixmap(":/Resources/add.png"))
        self.listView.installEventFilter(self)
        return

    def create_layout(self):        
        vlay1 = QVBoxLayout()
        vlay1.addWidget(self.roleLabel)
        vlay1.addWidget(self.nameLabel)
        hlay = QHBoxLayout()
        hlay.addLayout(vlay1,12)
        hlay.addWidget(self.signoutBtn)
        vlay = QVBoxLayout()
        vlay.addWidget(self.listView,12)
        vlay.addLayout(hlay)
        self.setLayout(vlay) 
        return
    def connect_signals(self):
        self.signoutBtn.clicked.connect(self.signout_clicked)
        self.addNewLabel.clicked.connect(self.add_new_requested)
        return

    def set_model(self, model):
        if model == None:
            return
        self.listView.setModel(model)
        self.listView.selectionModel().currentChanged.connect(self.on_current_changed)

    def set_current(self, current):
        index = self.listView.model().index(current, 0)
        self.listView.setCurrentIndex(index)

    def get_current(self):
        return self.listView.currentIndex().row()

    def set_user(self, user):
        if not user:
            return
        role = "Admin"
        if user.role == 3:
            self.addNewLabel.hide()
            role = "Client"
        elif user.role == 2:
            role = "Realtor"
        self.roleLabel.setText("Role: {}".format(role))
        self.nameLabel.setText("{} {}".format(user.first_name, user.last_name))
        return



    def on_current_changed(self, current, previous):
        self.current_changed.emit(current.row())



    def eventFilter(self, obj, event):
        if obj is self.listView and event.type() == event.Resize:
            self.alignTreeButton()       
        return False

    def alignTreeButton(self):
        padding = QSize(5,5) # optional
        newSize = self.listView.size() - self.addNewLabel.size() - padding
        self.addNewLabel.move(newSize.width(), newSize.height())  