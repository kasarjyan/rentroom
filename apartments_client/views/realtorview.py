from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from views.widget import Widget
from apartments.models import Apartment, User
from apartments.encoder import CustomJSONEncoder
from views.maps.mapsview import MapsView
from views.sidebar import SideBar
from signed_user import signedUser
from views.models import *
from views.editors import *
from views.dialogs import AddApartmentDialog
import json
from enum import Enum

class ActiveListView(Enum):
    ApartmentsList=0
    ClientsList=1
    RealtorsList=2

class RealtorView(Widget):
    signout_clicked = pyqtSignal()
    apartments_requested = pyqtSignal()
    realtors_requested = pyqtSignal()
    clients_requested  = pyqtSignal()
    delete_apartment_requested = pyqtSignal(int)
    update_apartment_requested = pyqtSignal(int, str)
    delete_client_requested = pyqtSignal(int)
    update_client_requested = pyqtSignal(int, str)
    delete_realtor_requested = pyqtSignal(int)
    update_realtor_requested = pyqtSignal(int, str)
    add_apartment_requested = pyqtSignal(str)

    def __init__(self, parent = None):
        super().__init__(parent)
        return

    def create_widgets(self):
        super().create_widgets()
        self.sidebar = SideBar()

        self.editorContainer = QWidget()
        self.editorLayout = QStackedLayout()

        self.apartmentEditor = ApartmentEditor()
        self.clientEditor = UserEditor()
        self.realtorEditor = UserEditor()

        self.mapsView = MapsView()
        self.apartmentsModel = ApartmentsModel()
        self.clientsModel = ClientsModel()
        self.realtorsModel = RealtorsModel()
        return

    def create_layout(self):
        super().create_layout()
        self.editorLayout.addWidget(self.apartmentEditor)
        self.editorLayout.addWidget(self.clientEditor)
        self.editorLayout.addWidget(self.realtorEditor)
        self.editorContainer.setLayout(self.editorLayout)
        self.editorContainer.hide()
        sideLayout = QVBoxLayout()
        sideLayout.addWidget(self.sidebar, 12)
        hlay = QHBoxLayout()
        hlay.addLayout(sideLayout)
        hlay.addWidget(self.editorContainer)
        hlay.addWidget(self.mapsView, 12)
        self.setLayout(hlay)
        return

    def showEvent(self, event):
        if not signedUser.user or not signedUser.token:
            return
        self.sidebar.set_user(signedUser.user)

    def connect_signals(self):
        super().connect_signals()
        self.sidebar.signout_clicked.connect(self.signout_clicked)
        self.sidebar.current_changed.connect(self.on_current_changed)
        self.sidebar.add_new_requested.connect(self.on_add_new_requested)
        self.mapsView.markerClicked.connect(self.on_marker_clicked)
        self.apartmentEditor.locationUpdated.connect(self.on_apartment_location_updated)
        self.apartmentEditor.delete.connect(self.on_delete_apartment_clicked)
        self.apartmentEditor.update.connect(self.on_update_apartment_clicked)

        self.clientEditor.delete.connect(self.on_delete_client_clicked)
        self.clientEditor.update.connect(self.on_update_client_clicked)
        
        self.realtorEditor.delete.connect(self.on_delete_realtor_clicked)
        self.realtorEditor.update.connect(self.on_update_realtor_clicked)
        return

    def on_apartments_selected(self):
        print("apartments")
        self.apartments_requested.emit()
        self.sidebar.set_model(self.apartmentsModel)
        self.editorContainer.hide()
        self.activeListView = ActiveListView.ApartmentsList
        return
    

    def on_get_apartments_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to get apartments",responseData.json["msg"], QMessageBox.Ok)
        else:
            apartments_array = responseData.json["apartments"]
            self.apartments = []
            for ap in apartments_array:
                a = Apartment.from_dict(ap)
                self.apartments.append(a)
            self.apartmentsModel.setApartmentsList(self.apartments)
            self.mapsView.setApartments(self.apartments)

    def on_get_clients_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to get clients",responseData.json["msg"], QMessageBox.Ok)
        else:
            clients_array = responseData.json["clients"]
            self.clients = []
            for cl in clients_array:
                a = User.from_dict(cl)
                self.clients.append(a)
            self.clientsModel.setClientsList(self.clients)

    def on_get_realtors_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to get realtors",responseData.json["msg"], QMessageBox.Ok)
        else:
            realtors_array = responseData.json["realtors"]
            self.realtors = []
            for rl in realtors_array:
                a = User.from_dict(rl)
                self.realtors.append(a)
            self.realtorsModel.setRealtorsList(self.realtors)
#update&delete apartment ready
    def on_delete_apartment_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to delete apartment",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.apartments_requested.emit()
            self.editorContainer.hide()

    def on_update_apartment_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to update apartment",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.apartments_requested.emit()

#update&delete client ready
    def on_client_apartment_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to delete client",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.clients_requested.emit()
            self.editorContainer.hide()

    def on_update_client_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to update client",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.clients_requested.emit()

#update&delete realtor ready
    def on_delete_realtor_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to delete realtor",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.realtors_requested.emit()
            self.editorContainer.hide()

    def on_update_realtor_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to update realtor",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.realtors_requested.emit()

    def on_current_changed(self, current):
        print("Current changed {}".format(current))
        self.editorLayout.setCurrentIndex(self.activeListView.value)
        if self.activeListView.value == 0:
            self.apartmentEditor.set_apartment(self.apartments[current])
            self.mapsView.panToApartment(self.apartments[current])
        if self.activeListView.value == 1:
            self.clientEditor.set_user(self.clients[current])
        if self.activeListView.value == 2:
            self.realtorEditor.set_user(self.realtors[current])
        self.editorContainer.show()

    def on_marker_clicked(self, id):
        current = None
        ind = 0
        for a in self.apartments:
            if a.id == id:
                current = ind
            ind +=1
        if current is not None and current != self.sidebar.get_current():
            self.sidebar.set_model(self.apartmentsModel)
            self.editorContainer.hide()
            self.activeListView = ActiveListView.ApartmentsList
            self.sidebar.set_current(current)



    def on_apartment_location_updated(self, apartment):
        self.mapsView.setApartments(self.apartments)
        self.mapsView.panToApartment(apartment)

#update&delete apartment
    def on_delete_apartment_clicked(self, apartment):
        self.delete_apartment_requested.emit(apartment.id)
    
    def on_update_apartment_clicked(self, id, apartment):
        jsonStr = json.dumps(apartment, cls=CustomJSONEncoder)
        self.update_apartment_requested.emit(id, jsonStr)

#update&delete client
    def on_update_client_clicked(self, id, client):
        jsonStr = json.dumps(client, cls=CustomJSONEncoder)
        self.update_client_requested.emit(id, jsonStr)

    def on_delete_client_clicked(self, client):
        self.delete_client_requested.emit(client.id)

#update&delete client    
    def on_update_realtor_clicked(self, id, realtor):
        jsonStr = json.dumps(realtor, cls=CustomJSONEncoder)
        self.update_realtor_requested.emit(id, jsonStr)


    def on_delete_realtor_clicked(self, realtor):
        self.delete_realtor_requested.emit(realtor.id)

    def on_add_apartment_ready(self, responseData):
        if not responseData.status:
            QMessageBox.critical(self, "Failed to create apartment",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.apartments_requested.emit()
            self.editorContainer.hide()
            QMessageBox.information(self, "Success","Apartment successfully created", QMessageBox.Ok)

    def on_add_new_requested(self):
        addApartmentDialog = AddApartmentDialog()
        addApartmentDialog.set_realtors([signedUser.user])
        if addApartmentDialog.exec():
            apartment = addApartmentDialog.apartment
            jsonStr = json.dumps(apartment, cls=CustomJSONEncoder)
            print ("Apartment json: {}".format(jsonStr))
            self.add_apartment_requested.emit(jsonStr)
        print("New Apartment Requested")