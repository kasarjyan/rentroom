from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from views.widget import Widget
from apartments.models import Apartment, User
from apartments.encoder import CustomJSONEncoder
from views.maps.mapsview import MapsView
from views.sidebar import SideBar
from signed_user import signedUser
from views.models import *
from views.editors import *
import json
from enum import Enum

class ClientView(Widget):
    signout_clicked = pyqtSignal()
    delete_client_requested = pyqtSignal(int)
    update_client_requested = pyqtSignal(int, str)
    apartments_requested = pyqtSignal()
    rent_requested=pyqtSignal('PyQt_PyObject')
    cancel_rent_requested=pyqtSignal('PyQt_PyObject')

    def __init__(self, parent = None):
        super().__init__(parent)
        return
    
    def create_widgets(self):
        super().create_widgets()
        self.sidebar = SideBar()

        self.editorContainer = QWidget()
        self.editorLayout = QStackedLayout()

        self.apartmentEditor = ApartmentEditor()
        self.clientEditor = UserEditor()

        self.mapsView = MapsView()
        self.apartmentsModel = ApartmentsModel()
        return

    def create_layout(self):
        super().create_layout()
        self.editorLayout.addWidget(self.apartmentEditor)
        self.editorLayout.addWidget(self.clientEditor)
        self.editorContainer.setLayout(self.editorLayout)
        self.editorContainer.hide()
        sideLayout = QVBoxLayout()
        sideLayout.addWidget(self.sidebar, 12)
        hlay = QHBoxLayout()
        hlay.addLayout(sideLayout)
        hlay.addWidget(self.editorContainer)
        hlay.addWidget(self.mapsView, 12)
        self.setLayout(hlay)
        return

    def showEvent(self, event):
        if not signedUser.user or not signedUser.token:
            return
        self.sidebar.set_user(signedUser.user)

    def connect_signals(self):
        super().connect_signals()
        self.sidebar.signout_clicked.connect(self.signout_clicked)
        self.sidebar.current_changed.connect(self.on_current_changed)
        
        self.mapsView.markerClicked.connect(self.on_marker_clicked)
        self.apartmentEditor.rent_requested.connect(self.on_rent_requested)
        self.apartmentEditor.cancel_rent_requested.connect(self.on_cancel_rent_requested)

        self.clientEditor.delete.connect(self.on_delete_client_clicked)
        self.clientEditor.update.connect(self.on_update_client_clicked)

        return
    
    def on_get_apartments_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to get apartments",responseData.json["msg"], QMessageBox.Ok)
        else:
            apartments_array = responseData.json["apartments"]
            self.apartments = []
            for ap in apartments_array:
                a = Apartment.from_dict(ap)
                self.apartments.append(a)
            self.apartmentsModel.setApartmentsList(self.apartments)            
            self.sidebar.set_model(self.apartmentsModel)
            self.mapsView.setApartments(self.apartments)

    def on_rent_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to rent apartment",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.apartments_requested.emit()

    def on_cancel_rent_ready(self, responseData):        
        if not responseData.status:
            QMessageBox.critical(self, "Failed to cancel apartment rent ",responseData.json["msg"], QMessageBox.Ok)
        else:
            self.apartments_requested.emit()

    def on_current_changed(self, current):
        print("Current changed {}".format(current))
        self.apartmentEditor.set_apartment(self.apartments[current])
        self.mapsView.panToApartment(self.apartments[current])        
        self.editorContainer.show()

    def on_marker_clicked(self, id):
        current = None
        ind = 0
        for a in self.apartments:
            if a.id == id:
                current = ind
            ind +=1
        if current is not None and current != self.sidebar.get_current():
            self.sidebar.set_model(self.apartmentsModel)
            self.editorContainer.hide()
            self.sidebar.set_current(current)

#update&delete apartment
    def on_rent_requested(self, rental):
        jsonStr = json.dumps(rental, cls=CustomJSONEncoder)
        #TODO: rent or cancel rent
        self.rent_requested.emit(jsonStr)

    def on_cancel_rent_requested(self, rental):
        jsonStr = json.dumps(rental, cls=CustomJSONEncoder)
        #TODO: rent or cancel rent
        self.cancel_rent_requested.emit(jsonStr)

#update&delete client
    def on_update_client_clicked(self, id, client):
        jsonStr = json.dumps(client, cls=CustomJSONEncoder)
        self.update_client_requested.emit(id, jsonStr)

    def on_delete_client_clicked(self, client):
        self.delete_client_requested.emit(client.id)

