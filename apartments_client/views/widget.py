from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtGui import QMouseEvent
from PyQt5.QtCore import pyqtSignal


class Widget(QWidget):    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.create_widgets()        
        self.create_layout()
        self.connect_signals()

    def create_widgets(self):
        return

    def create_layout(self):
        return

    def connect_signals(self):
        return

class ClickableLabel(QLabel):
    clicked = pyqtSignal()
    def __init__(self, parent=None):
        super().__init__(parent)

    def mousePressEvent(self, event:QMouseEvent):
        self.clicked.emit()
