from PyQt5.QtWidgets import QMainWindow, QStackedLayout, QHBoxLayout, QVBoxLayout, QListWidget, QWidget

from views.clientview import ClientView
from views.realtorview import RealtorView
from views.adminview import AdminView

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.create_widgets()
        self.create_layout()
        self.connect_signals()
        return

    def create_widgets(self):
        self.stacked_layout = QStackedLayout()

        self._clientView = ClientView()
        self._realtorView = RealtorView()
        self._adminView = AdminView()        
        
        centralWidget = QWidget()
        self.setCentralWidget(centralWidget)
        return

    def create_layout(self):

        self.stacked_layout.addWidget(self._clientView)
        self.stacked_layout.addWidget(self._realtorView)
        self.stacked_layout.addWidget(self._adminView)
        self.centralWidget().setLayout(self.stacked_layout)
        return

    def connect_signals(self):
        return
    
    def setClientCurrentView(self):
        self.stacked_layout.setCurrentIndex(0)
    
    def setRealtorCurrentView(self):
        self.stacked_layout.setCurrentIndex(1)
        
    def setAdminCurrentView(self):
        self.stacked_layout.setCurrentIndex(2)
    