from views.widget import Widget
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from apartments.models import User

class UserModel(QAbstractTableModel):
    def __init__(self, parent=None):
        super(UserModel, self).__init__(parent)
        self.rowsName = ['ID', 'First Name','Last Name','E-mail', 'Password']
        self.user = None
    
    def rowCount(self, parent=QModelIndex()):
        return len(self.rowsName)

    def columnCount(self, parent=QModelIndex()):
        return 2
    def flags(self, index=QModelIndex()):
        flags = super().flags(index);
        row = index.row()
        if index.column() == 1 and (row != 0 and row != 3):
            return flags | Qt.ItemIsEditable
        return flags
    
    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None
        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                if index.row() >= len(self.rowsName):
                    return None
                return self.rowsName[index.row()]
    
            if index.column() == 1 and self.user is not None:
                row = index.row()
                if row == 0:
                    return self.user.id
                elif row == 1:
                    return self.user.first_name
                elif row == 2:
                    return self.user.last_name
                elif row == 3:
                    return self.user.email
                elif row == 4:
                    return "***********"
            return None
        
        if role == Qt.BackgroundRole:
            batch = (index.row() // 100) % 2
            if batch == 0:
                return qApp.palette().base()
            return qApp.palette().alternateBase()    
        return None
    def setData(self, index, value, role = Qt.EditRole):
        result = False
        if index.column() == 1 and self.user is not None:
                row = index.row()
                if row == 1:
                    self.user.first_name = value
                    self.updated_dict['firstName'] = value
                    result = True
                elif row == 2:
                    self.user.last_name = value
                    self.updated_dict['lastName'] = value
                    result = True
                elif row == 4:
                    self.user.password = value
                    self.updated_dict['password'] = value
                    result = True
        if result:
            self.dataChanged.emit(self.createIndex(0,0), self.createIndex(0,0))
        return result

    
    def setUser(self, user):
        self.beginResetModel()
        self.updated_dict= {}
        self.user = user
        self.endResetModel()


class UserEditor(Widget):
    delete = pyqtSignal('PyQt_PyObject')
    update = pyqtSignal(int, 'PyQt_PyObject')
    def __init__(self, parent = None):
        super().__init__(parent)
        return
    def create_widgets(self):
        super().create_widgets()
        self.userModel = UserModel()
        self.userView = QTableView()
        self.userView.setModel(self.userModel)
        self.userView.horizontalHeader().hide();
        self.userView.verticalHeader().hide();
        self.userView.horizontalHeader().setStretchLastSection(True)
        self.userView.setFixedHeight(200)
        self.updateBtn = QPushButton("Update")
        self.deleteBtn = QPushButton("Delete")
        return

    def create_layout(self):
        super().create_layout()
        lay = QVBoxLayout()
        lay.addWidget(self.userView)
        hlay2 = QHBoxLayout()
        hlay2.addWidget(self.deleteBtn)
        hlay2.addStretch(12)
        hlay2.addWidget(self.updateBtn)
        lay.addLayout(hlay2)
        lay.addStretch(12)
        self.setLayout(lay)
        return

    def connect_signals(self):
        super().connect_signals()
        self.deleteBtn.clicked.connect(self.on_delete)
        self.updateBtn.clicked.connect(self.on_update)
        return

    def set_user(self, user):
        self.userModel.setUser(user)
    
    def on_delete(self):
        print("Delete User")
        self.delete.emit(self.userModel.user)

    def on_update(self):
        print("Update user")
        if len(self.userModel.updated_dict) > 0:
            user = User.from_dict(self.userModel.updated_dict)
            self.update.emit(self.userModel.user.id, user)
    