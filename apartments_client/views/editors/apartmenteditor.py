from views.widget import Widget
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from dateutil.parser import parse
from views.dialogs import PickLocationDialog, PickDatesDialog
from apartments.models import Apartment, Rental
from signed_user import signedUser


class ApartmentModel(QAbstractTableModel):
    def __init__(self, parent=None):
        super(ApartmentModel, self).__init__(parent)
        self.rowsName = ['Apartment ID', 'Name','Description','Floor Area','Price','Rooms','Geo Position','Date Added','Realtor ID','Status']
        self.apartment = None
    
    def rowCount(self, parent=QModelIndex()):
        return len(self.rowsName)

    def columnCount(self, parent=QModelIndex()):
        return 2

    def flags(self, index=QModelIndex()):
        flags = super().flags(index);
        row = index.row()

        if index.column() == 1 and (row != 0 and row != 7 and row != 6 and row != 8 and row != 9) and not signedUser.user.is_client:
            return flags | Qt.ItemIsEditable
        return flags
    
    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None
        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                if index.row() >= len(self.rowsName):
                    return None
                return self.rowsName[index.row()]
    
            if index.column() == 1 and self.apartment is not None:
                row = index.row()
                if row == 0:
                    return self.apartment.id
                elif row == 1:
                    return self.apartment.name
                elif row == 2:
                    return self.apartment.description
                elif row == 3:
                    return self.apartment.floor_area
                elif row == 4:
                    return self.apartment.price
                elif row == 5:
                    return self.apartment.rooms_num
                elif row == 6:
                    return "({0:.2f}, {1:.2f})".format(self.apartment.latitude,self.apartment.longitude)
                elif row == 7:
                    datestring = self.apartment.date_added
                    return QDate.fromString(datestring.strftime('%d-%m-%Y'), "dd-MM-yyyy")
                elif row == 8:
                    return self.apartment.realtorid
                elif row == 9:
                    return self.apartment.status
            return None
        
        if role == Qt.BackgroundRole:
            batch = (index.row() // 100) % 2
            if batch == 0:
                return qApp.palette().base()
            return qApp.palette().alternateBase()
    
        return None
    def setData(self, index, value, role = Qt.EditRole):
        result = False
        if index.column() == 1 and self.apartment is not None:
                row = index.row()
                if row == 1:
                    self.apartment.name = value
                    self.updated_dict['name'] = value
                    result = True
                elif row == 2:
                    self.apartment.description = value
                    self.updated_dict['description'] = value
                    result = True
                elif row == 3:
                    self.apartment.floor_area = float(value)
                    self.updated_dict['floorArea'] = float(value)
                    result = True
                elif row == 4:
                    self.apartment.price = float(value)
                    self.updated_dict['price'] = float(value)
                    result = True
                elif row == 5:
                    self.apartment.rooms_num = int(value)
                    self.updated_dict['roomsNum'] = float(value)
                    result = True
        if result:
            self.dataChanged.emit(self.createIndex(0,0), self.createIndex(0,0))
        return result

    
    def setApartment(self, apartment):
        self.beginResetModel()
        self.updated_dict= {}
        self.apartment = apartment
        self.endResetModel()
    
class ApartmentEditor(Widget):
    locationUpdated = pyqtSignal('PyQt_PyObject')
    delete = pyqtSignal('PyQt_PyObject')
    update = pyqtSignal(int, 'PyQt_PyObject')
    rent_requested = pyqtSignal('PyQt_PyObject')
    cancel_rent_requested = pyqtSignal('PyQt_PyObject')

    def __init__(self, parent = None):
        super().__init__(parent)
        return
    def create_widgets(self):
        super().create_widgets()
        self.apartmentModel = ApartmentModel()
        self.apartmentView = QTableView()
        self.apartmentView.setModel(self.apartmentModel)
        self.apartmentView.horizontalHeader().hide();
        self.apartmentView.verticalHeader().hide();
        self.apartmentView.horizontalHeader().setStretchLastSection(True)
        self.apartmentView.setFixedHeight(333)
        self.updateBtn = QPushButton("Update")
        self.deleteBtn = QPushButton("Delete")
        self.pickLocation = QLabel("Pick Location")
        self.pickLocation.setText("<a href=\"http://example.com/\">Pick Location</a>")
        self.pickLocation.setTextFormat(Qt.RichText)
        self.pickLocation.setTextInteractionFlags(Qt.TextBrowserInteraction)
        return

    def create_layout(self):
        super().create_layout()
        lay = QVBoxLayout()
        lay.addWidget(self.apartmentView)
        hlay1 = QHBoxLayout()
        hlay1.addWidget(self.pickLocation)
        lay.addLayout(hlay1)
        hlay2 = QHBoxLayout()
        hlay2.addWidget(self.deleteBtn)
        hlay2.addStretch(12)
        hlay2.addWidget(self.updateBtn)
        lay.addLayout(hlay2)
        lay.addStretch(12)
        self.setLayout(lay)
        return

    def connect_signals(self):
        super().connect_signals()
        self.pickLocation.linkActivated.connect(self.on_pick_location)
        self.deleteBtn.clicked.connect(self.on_delete)
        self.updateBtn.clicked.connect(self.on_update)
        return
    def set_apartment(self, apartment):
        self.apartmentModel.setApartment(apartment)
        if signedUser.user.is_client:
            self.deleteBtn.hide()
            self.pickLocation.hide()
            if apartment.is_available:
                self.updateBtn.setText('Rent')
            else:
                self.updateBtn.setText('Cancel Rent')

    
    def on_pick_location(self, link):
        pickLocationDialog = PickLocationDialog()
        pickLocationDialog.setApartment(self.apartmentModel.apartment)
        if pickLocationDialog.exec():
            apartment = self.apartmentModel.apartment
            apartment.latitude = pickLocationDialog.latitude
            apartment.longitude = pickLocationDialog.longitude
            updated_dict =self.apartmentModel.updated_dict
            updated_dict['latitude'] = pickLocationDialog.latitude
            updated_dict['longitude'] = pickLocationDialog.longitude
            self.apartmentModel.setApartment(apartment)
            self.apartmentModel.updated_dict = updated_dict
            self.locationUpdated.emit(apartment)
        print("Link activated {}".format(link))

    def on_delete(self):
        print("Delete apartment")
        self.delete.emit(self.apartmentModel.apartment)
    def on_update(self):
        if signedUser.user.is_client:
            self.on_update_rent()
        else:
            print("Update apartment")
            if len(self.apartmentModel.updated_dict) > 0:
                apartment = Apartment.from_dict(self.apartmentModel.updated_dict)
                self.update.emit(self.apartmentModel.apartment.id, apartment)
    
    def on_update_rent(self):
        if self.apartmentModel.apartment.is_available:
            pickDatesDialog = PickDatesDialog()
            if pickDatesDialog.exec():
                rental = Rental()
                rental.apartmentid = self.apartmentModel.apartment.id
                rental.clientid = signedUser.user.id
                rental.start_date = parse(pickDatesDialog.start_date.toString())
                rental.end_date = parse(pickDatesDialog.end_date.toString())
                self.rent_requested.emit(rental)
        else:                
            rental = Rental()
            rental.apartmentid = self.apartmentModel.apartment.id
            rental.clientid = signedUser.user.id
            self.cancel_rent_requested.emit(rental)