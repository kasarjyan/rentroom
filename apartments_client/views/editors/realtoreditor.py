from views.widget import Widget
from PyQt5.QtWidgets import *

class RealtorEditor(Widget):
    def __init__(self, parent = None):
        super().__init__(parent)
        return

    def create_widgets(self):
        super().create_widgets()
        return

    def create_layout(self):
        super().create_layout()
        button = QPushButton("Realtor")
        hlay = QHBoxLayout()
        hlay.addWidget(button)
        self.setLayout(hlay)
        return

    def connect_signals(self):
        super().connect_signals()
        return