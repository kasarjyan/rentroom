from PyQt5.QtCore import *
from PyQt5.QtWidgets import qApp
from apartments.models import User

class RealtorsModel(QAbstractListModel):
    def __init__(self, parent=None):
        super(RealtorsModel, self).__init__(parent)

        self.realtorsCount = 0
        self.realtorsList = []

    def rowCount(self, parent=QModelIndex()):
        return self.realtorsCount

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if index.row() >= len(self.realtorsList) or index.row() < 0:
            return None

        if role == Qt.DisplayRole:
            return "{} {}".format(self.realtorsList[index.row()].first_name, self.realtorsList[index.row()].last_name)

        if role == Qt.BackgroundRole:
            batch = (index.row() // 100) % 2
            if batch == 0:
                return qApp.palette().base()
            return qApp.palette().alternateBase()

        return None

    def canFetchMore(self, index):
        return self.realtorsCount < len(self.realtorsList)

    def fetchMore(self, index):
        remainder = len(self.realtorsList) - self.realtorsCount
        itemsToFetch = min(100, remainder)

        self.beginInsertRows(QModelIndex(), self.realtorsCount,
                self.realtorsCount + itemsToFetch)

        self.realtorsCount += itemsToFetch

        self.endInsertRows()

    def setRealtorsList(self, realtorsList):
        self.beginResetModel()
        self.realtorsList = realtorsList
        self.realtorsCount = 0
        self.endResetModel()