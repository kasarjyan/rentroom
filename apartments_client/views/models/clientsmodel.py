from PyQt5.QtCore import *
from PyQt5.QtWidgets import qApp
from apartments.models import User

class ClientsModel(QAbstractListModel):
    def __init__(self, parent=None):
        super(ClientsModel, self).__init__(parent)

        self.clientsCount = 0
        self.clientsList = []

    def rowCount(self, parent=QModelIndex()):
        return self.clientsCount

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if index.row() >= len(self.clientsList) or index.row() < 0:
            return None

        if role == Qt.DisplayRole:
            return "{} {}".format(self.clientsList[index.row()].first_name, self.clientsList[index.row()].last_name)

        if role == Qt.BackgroundRole:
            batch = (index.row() // 100) % 2
            if batch == 0:
                return qApp.palette().base()
            return qApp.palette().alternateBase()

        return None

    def canFetchMore(self, index):
        return self.clientsCount < len(self.clientsList)

    def fetchMore(self, index):
        remainder = len(self.clientsList) - self.clientsCount
        itemsToFetch = min(100, remainder)

        self.beginInsertRows(QModelIndex(), self.clientsCount,
                self.clientsCount + itemsToFetch)

        self.clientsCount += itemsToFetch

        self.endInsertRows()

    def setClientsList(self, clientsList):

        self.beginResetModel()
        self.clientsList = clientsList
        self.clientsCount = 0
        self.endResetModel()