from PyQt5.QtCore import *
from PyQt5.QtWidgets import qApp
from apartments.models import Apartment

class ApartmentsModel(QAbstractListModel):
    def __init__(self, parent=None):
        super(ApartmentsModel, self).__init__(parent)

        self.apartmentsCount = 0
        self.apartmentsList = []

    def rowCount(self, parent=QModelIndex()):
        return self.apartmentsCount

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None

        if index.row() >= len(self.apartmentsList) or index.row() < 0:
            return None

        if role == Qt.DisplayRole:
            return self.apartmentsList[index.row()].name

        if role == Qt.BackgroundRole:
            batch = (index.row() // 100) % 2
            if batch == 0:
                return qApp.palette().base()
            return qApp.palette().alternateBase()

        return None

    def canFetchMore(self, index):
        return self.apartmentsCount < len(self.apartmentsList)

    def fetchMore(self, index):
        remainder = len(self.apartmentsList) - self.apartmentsCount
        itemsToFetch = min(100, remainder)

        self.beginInsertRows(QModelIndex(), self.apartmentsCount,
                self.apartmentsCount + itemsToFetch)

        self.apartmentsCount += itemsToFetch

        self.endInsertRows()

    def setApartmentsList(self, apartmentsList):

        self.beginResetModel()
        self.apartmentsList = apartmentsList
        self.apartmentsCount = 0
        self.endResetModel()