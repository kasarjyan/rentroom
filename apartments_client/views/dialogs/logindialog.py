import sys
import json

from PyQt5.QtWidgets import QApplication, QPushButton, QLineEdit, QLabel, QDialog, QComboBox, QFormLayout, QVBoxLayout, QHBoxLayout
from PyQt5.QtCore import QObject,  QMetaObject, Q_ARG, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QMessageBox
from apartments.models import User
from signed_user import signedUser


class LoginDialog(QDialog):
    signin_requested = pyqtSignal('PyQt_PyObject')
    signup_requested = pyqtSignal('PyQt_PyObject')
    user_requested = pyqtSignal(int, int, str)
    signin_success = pyqtSignal('PyQt_PyObject')

    def __init__(self, parent=None):
        super().__init__(parent)
        self.create_widgets()
        self.create_layout()
        self.connect_signals()

    def create_widgets(self):        
        self._signinEmailLineEdit = QLineEdit()
        self._signinPasswordLineEdit = QLineEdit()
        self._signinBtn = QPushButton()

        self._firstNameLineEdit = QLineEdit()
        self._lastNameLineEdit = QLineEdit()
        self._signupEmailLineEdit = QLineEdit()
        self._signupPasswordLineEdit = QLineEdit()
        self._userRoleComboBox = QComboBox()
        
        self._signupBtn = QPushButton()

        #Setup Widgets
        self._signinPasswordLineEdit.setEchoMode(QLineEdit.Password)
        self._signupPasswordLineEdit.setEchoMode(QLineEdit.Password)

        self._signinBtn.setText("Sign In")
        self._signupBtn.setText("Sign Up")
        self._userRoleComboBox.addItems(("Admin", "Realtor", "Client"))


        self._signinEmailLineEdit.setText("hov@gmail.com")
        self._signinPasswordLineEdit.setText("kasarjyan")


    def create_layout(self):
        formLayout = QFormLayout()
        signinLayout = QHBoxLayout()
        signinLayout.addWidget(QLabel("Email:"))
        signinLayout.addWidget(self._signinEmailLineEdit)
        signinLayout.addWidget(QLabel("Password:"))
        signinLayout.addWidget(self._signinPasswordLineEdit)
        signinLayout.addWidget(self._signinBtn)
        formLayout.addRow("First Name", self._firstNameLineEdit)
        formLayout.addRow("Last Name", self._lastNameLineEdit)
        formLayout.addRow("Email", self._signupEmailLineEdit)
        formLayout.addRow("Password", self._signupPasswordLineEdit)
        formLayout.addRow("Role", self._userRoleComboBox)
        formLayout.addRow(self._signupBtn)
        hlay = QHBoxLayout()
        hlay.addStretch(2)
        hlay.addLayout(formLayout,4)
        hlay.addStretch(2)
        lay = QVBoxLayout()
        lay.addLayout(signinLayout)
        lay.addStretch(1)
        lay.addLayout(hlay,1)
        lay.addStretch(1)
        self.setLayout(lay)
        self.setFixedSize(480,320)

    def connect_signals(self):
        self._signinBtn.clicked.connect(self.signin_clicked)
        self._signupBtn.clicked.connect(self.signup_clicked)
    

    def signin_clicked(self):
        user  = User()
        user.email = self._signinEmailLineEdit.text()
        user.password = self._signinPasswordLineEdit.text()
        self.signin_requested.emit(user)
        

    def signup_clicked(self):
        user  = User()
        user.email = self._signupEmailLineEdit.text()
        user.password = self._signupPasswordLineEdit.text()
        user.first_name = self._firstNameLineEdit.text()
        user.last_name = self._lastNameLineEdit.text()
        user.role = self._userRoleComboBox.currentIndex()+1
        self.signup_requested.emit(user)
        #responseData = _client.signup(user)
        #if not responseData.status:
        #    QMessageBox.critical("adasdasd",responseData.json["msg"])


    @pyqtSlot('PyQt_PyObject')
    def on_login_ready(self, responseData):
        if not responseData.status:
            QMessageBox.critical(self, "Failed",responseData.json["msg"], QMessageBox.Ok)
        else:
            id = int(responseData.json["id"])
            role = int(responseData.json["role"])
            token = str(responseData.json["access_token"])
            signedUser.token = token
            self.user_requested.emit(id, role, token)

    @pyqtSlot('PyQt_PyObject')
    def on_user_ready(self, responseData):
        if not responseData.status:
            QMessageBox.critical(self, "Failed",responseData.json["msg"], QMessageBox.Ok)
        else:
            user = User.from_dict(responseData.json)
            signedUser.user = user
            self.signin_success.emit(user)


    @pyqtSlot('PyQt_PyObject')
    def on_signup_ready(self, responseData):
         if not responseData.status:
            QMessageBox.critical(self, "Failed",responseData.json["msg"], QMessageBox.Ok)
         else:
            QMessageBox.information(self, "Success", "User successfully created", QMessageBox.Ok)

        


