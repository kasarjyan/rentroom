import sys
import json
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class PickDatesDialog(QDialog):
    def __init__(self, parent=None):
        super(PickDatesDialog,self).__init__(parent)

        self.create_widgets()
        self.create_layout()
        self.connect_signals()

    def create_widgets(self):
        icon = QIcon(":/Resources/calendar.png")
        self.startDate = QLineEdit()
        self.selectStartDateAction = self.startDate.addAction(icon, QLineEdit.TrailingPosition)
        self.endDate = QLineEdit()
        self.selectEndDateAction = self.endDate.addAction(icon, QLineEdit.TrailingPosition)

        self.startDate.setReadOnly(True)
        self.endDate.setReadOnly(True)
        self.startDate.setText(QDate.currentDate().toString())
        self.endDate.setText(QDate.currentDate().addDays(7).toString())
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        
        self.end_date = QDate.currentDate()
        self.start_date = QDate.currentDate().addDays(7)

    def create_layout(self):
        hlay = QHBoxLayout()
        vlay = QVBoxLayout()
        formLayout = QFormLayout()
        formLayout.addRow("Start Date", self.startDate,)
        formLayout.addRow("End Date", self.endDate)
        hlay.addStretch(3)
        hlay.addLayout(formLayout)
        hlay.addStretch(2)
        vlay.addLayout(hlay)
        vlay.addWidget(self.buttonBox)
        self.setLayout(vlay)
        self.setFixedSize(320,240)

    def connect_signals(self):
        self.selectStartDateAction.triggered.connect(self.on_select_start_date)
        self.selectEndDateAction.triggered.connect(self.on_select_end_date)
        self.buttonBox.accepted.connect(self.accept);
        self.buttonBox.rejected.connect(self.reject);


    def select_date(self, id):
        dateDialod = QDialog()
        calendar = QCalendarWidget()
        calendar.setMinimumDate(QDate.fromString(self.startDate.text()))
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        vbox = QVBoxLayout()
        vbox.addWidget(calendar)
        vbox.addWidget(buttonBox)
        dateDialod.setLayout(vbox)        
        dateDialod.setWindowTitle('Calendar')
        
        buttonBox.accepted.connect(dateDialod.accept);
        buttonBox.rejected.connect(dateDialod.reject);
        if dateDialod.exec():
            return calendar.selectedDate()
        return None


    def on_select_start_date(self):
        selected_date = self.select_date(0)
        if selected_date is not None:
            self.start_date = selected_date
            self.startDate.setText(selected_date.toString())

    def on_select_end_date(self):
        selected_date = self.select_date(1)
        if selected_date is not None:
            self.end_date = select_date
            self.endDate.setText(selected_date.toString())
