import sys
import json

from PyQt5.QtWidgets import QApplication, QPushButton, QLineEdit, QLabel, QDialog, QComboBox, QFormLayout, QVBoxLayout, QHBoxLayout,QDialogButtonBox
from PyQt5.QtCore import QObject,  QMetaObject, Q_ARG, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QMessageBox
from apartments.models import User


class AddUserDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.create_widgets()
        self.create_layout()
        self.connect_signals()

    def create_widgets(self):
        self._firstNameLineEdit = QLineEdit()
        self._lastNameLineEdit = QLineEdit()
        self._signupEmailLineEdit = QLineEdit()
        self._signupPasswordLineEdit = QLineEdit()
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

    def create_layout(self):
        formLayout = QFormLayout()
        formLayout.addRow("First Name", self._firstNameLineEdit)
        formLayout.addRow("Last Name", self._lastNameLineEdit)
        formLayout.addRow("Email", self._signupEmailLineEdit)
        formLayout.addRow("Password", self._signupPasswordLineEdit)    
        hlay = QHBoxLayout()
        hlay.addStretch(2)
        hlay.addLayout(formLayout,3)
        hlay.addStretch(2)
        lay = QVBoxLayout()
        lay.addStretch(1)
        lay.addLayout(hlay,1)
        lay.addStretch(1)
        lay.addWidget(self.buttonBox)
        self.setLayout(lay)
        self.setFixedSize(480,320)

    def connect_signals(self):
        self.buttonBox.accepted.connect(self.on_accept);
        self.buttonBox.rejected.connect(self.reject);

    def on_accept(self):
        self.user  = User()
        self.user.email = self._signupEmailLineEdit.text()
        self.user.password = self._signupPasswordLineEdit.text()
        self.user.first_name = self._firstNameLineEdit.text()
        self.user.last_name = self._lastNameLineEdit.text()
        self.accept()



