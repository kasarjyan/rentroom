import sys
import json
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from views.maps.mapsview import MapsView


class PickLocationDialog(QDialog):    
    location_picked = pyqtSignal(float, float)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.create_widgets()
        self.create_layout()
        self.connect_signals()
        self.apartment= None

    def setApartment(self, apartment):
        self.apartment = apartment

    def create_widgets(self):        
        self.mapsView = MapsView()

    def create_layout(self):        
        lay = QVBoxLayout()
        lay.addWidget(self.mapsView)
        self.setLayout(lay)
        self.setFixedSize(640,480)

    def connect_signals(self):
        self.mapsView.loadFinished.connect(self.onLoadFinished)
        self.mapsView.locationPicked.connect(self.onLocationPicked)

    def onLoadFinished(self, ok):
        if self.apartment is not None:
            self.mapsView.setApartments([self.apartment])
    def onLocationPicked(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude
        self.accept()
    

        


