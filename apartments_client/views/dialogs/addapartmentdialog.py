import sys
import json
from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import *
from PyQt5.QtCore import QObject,  QMetaObject, Q_ARG, pyqtSignal, pyqtSlot
from apartments.models import Apartment, User
from views.dialogs import PickLocationDialog

class AddApartmentDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.create_widgets()
        self.create_layout()
        self.connect_signals()

    def create_widgets(self):
        self._nameLineEdit = QLineEdit()
        self._descriptionLineEdit = QTextEdit()
        self._floorAreaSpinBox = QDoubleSpinBox()
        self._priceSpinBox = QDoubleSpinBox()
        self._roomsSpinBox = QSpinBox()
        self._geoposLineEdit = QLineEdit()
        self._realtorsComboBox = QComboBox()
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.latitude =  None
        self.longitude = None
        
        self._floorAreaSpinBox.setMinimum(14.0)
        self._priceSpinBox.setMinimum(1.0)
        self._roomsSpinBox.setMinimum(1)
        
        self._floorAreaSpinBox.setMaximum(2000.0)
        self._priceSpinBox.setMaximum(10000.0)
        self._roomsSpinBox.setMaximum(200)

        icon = QIcon(":/Resources/pick_location.png")
        self.pickLocationAction = self._geoposLineEdit.addAction(icon, QLineEdit.TrailingPosition)
        self._geoposLineEdit.setReadOnly(True)

    def create_layout(self):
        formLayout = QFormLayout()
        formLayout.addRow("Name", self._nameLineEdit)
        formLayout.addRow("Description", self._descriptionLineEdit)
        formLayout.addRow("Floot Area", self._floorAreaSpinBox)
        formLayout.addRow("Rooms", self._roomsSpinBox)
        formLayout.addRow("Price", self._priceSpinBox)
        formLayout.addRow("Geo Position", self._geoposLineEdit)
        formLayout.addRow("Realtor", self._realtorsComboBox)
        hlay = QHBoxLayout()
        hlay.addStretch(2)
        hlay.addLayout(formLayout,3)
        hlay.addStretch(2)
        lay = QVBoxLayout()
        lay.addStretch(1)
        lay.addLayout(hlay,1)
        lay.addStretch(1)
        lay.addWidget(self.buttonBox)
        self.setLayout(lay)
        self.setFixedSize(480,320)

    def connect_signals(self):
        self.buttonBox.accepted.connect(self.on_accept);
        self.buttonBox.rejected.connect(self.reject);
        self.pickLocationAction.triggered.connect(self.on_pick_location)

    def set_realtors(self, realtors):
        self.realtors = realtors
        for realtor in self.realtors:
            self._realtorsComboBox.addItem("{} {}".format(realtor.first_name, realtor.last_name), userData=realtor.id)

    def on_accept(self):
        self.apartment  = Apartment()
        self.apartment.name = self._nameLineEdit.text()
        self.apartment.description = self._descriptionLineEdit.text()
        self.apartment.floor_area = self._floorAreaSpinBox.value()
        self.apartment.rooms_num = self._roomsSpinBox.value()
        self.apartment.price = self._priceSpinBox.value()
        if self.latitude is not None:
            self.apartment.latitude = self.latitude
        if self.longitude is not None:
            self.apartment.longitude = self.longitude
        self.apartment.realtorid = self._realtorsComboBox.currentData()
        self.accept()

    def on_pick_location(self):
        pickLocationDialog = PickLocationDialog()
        if pickLocationDialog.exec():
            self.latitude = pickLocationDialog.latitude
            self.longitude = pickLocationDialog.longitude
            self._geoposLineEdit.setText( "({0:.2f}, {1:.2f})".format(self.latitude, self.longitude))




