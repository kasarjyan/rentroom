# coding: utf-8

# flake8: noqa
from __future__ import absolute_import
# import models into model package
from apartments.models.apartment import Apartment
from apartments.models.permissions import Permissions
from apartments.models.rental import Rental
from apartments.models.user import User
