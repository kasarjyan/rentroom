import datetime
import os
import six
import json
import typing
import hashlib
import binascii

def _deserialize(data, klass):
    """Deserializes dict, list, str into an object.

    :param data: dict, list or str.
    :param klass: class literal, or string of class name.

    :return: object.
    """
    if data is None:
        return None

    if klass in six.integer_types or klass in (float, str, bool):
        return _deserialize_primitive(data, klass)
    elif klass == object:
        return _deserialize_object(data)
    elif klass == datetime.date:
        return deserialize_date(data)
    elif klass == datetime.datetime:
        return deserialize_datetime(data)
    elif type(klass) == typing._GenericAlias:
        if klass.__origin__ == list:
            return _deserialize_list(data, klass.__args__[0])
        if klass.__origin__ == dict:
            return _deserialize_dict(data, klass.__args__[1])
    else:
        return deserialize_model(data, klass)


def _deserialize_primitive(data, klass):
    """Deserializes to primitive type.

    :param data: data to deserialize.
    :param klass: class literal.

    :return: int, long, float, str, bool.
    :rtype: int | long | float | str | bool
    """
    try:
        value = klass(data)
    except UnicodeEncodeError:
        value = six.u(data)
    except TypeError:
        value = data
    return value


def _deserialize_object(value):
    """Return a original value.

    :return: object.
    """
    return value


def deserialize_date(string):
    """Deserializes string to date.

    :param string: str.
    :type string: str
    :return: date.
    :rtype: date
    """
    try:
        from dateutil.parser import parse
        return parse(string).date()
    except BaseException:
        return string



def deserialize_datetime(string):
    """Deserializes string to datetime.

    The string should be in iso8601 datetime format.

    :param string: str.
    :type string: str
    :return: datetime.
    :rtype: datetime
    """
    try:
        from dateutil.parser import parse
        return parse(string)
    except BaseException:
        return string

def deserialize_model(data, klass):
    """Deserializes list or dict to model.

    :param data: dict, list.
    :type data: dict | list
    :param klass: class literal.
    :return: model object.
    """
    instance = klass()

    if not instance.swagger_types:
        return data

    for attr, attr_type in six.iteritems(instance.swagger_types):
        if data is not None \
                and instance.attribute_map[attr] in data \
                and isinstance(data, (list, dict)):
            value = data[instance.attribute_map[attr]]
            setattr(instance, attr, _deserialize(value, attr_type))

    return instance

def generate_insert_query(obj, table_name):
    klass = type(obj)
    if not obj.swagger_types:
        return None, None
    query="INSERT INTO {}( ".format(table_name)    
    for attr, attr_type in six.iteritems(obj.swagger_types):
        if attr is not 'id':
            query += obj.attribute_map[attr]
            query += ','
    query = query[:-1]
    query += ") VALUES("
    values_dict={}
    for attr, attr_type in six.iteritems(obj.swagger_types):
        if attr is not 'id':
            query += "%({}){},".format(obj.attribute_map[attr], 's')
            values_dict[obj.attribute_map[attr]] = value = getattr(obj, attr)
    query = query[:-1]
    query+=');'
    return query, values_dict


def generate_update_query(obj, table_name):
    klass = type(obj)
    # UPDATE TBL SET  col1=val1, col2=val2 WHERE col3=val3
    if not obj.swagger_types:
        return None, None
    query="UPDATE {} SET ".format(table_name)    
    values_dict={}
    for attr, attr_type in six.iteritems(obj.swagger_types):
        if attr is not 'id' and getattr(obj, attr) is not None:
            query +="{}=%({})s,".format(obj.attribute_map[attr], obj.attribute_map[attr])
            values_dict[obj.attribute_map[attr]] = getattr(obj, attr)
    query = query[:-1]
    query += " WHERE id={}".format(getattr(obj, 'id'))
    return query, values_dict

def generate_select_apartments_query(filters, client_id):
    query="SELECT * FROM apartments "
    
    condition="WHERE "
    if "realtorid" in filters:
        val = filters['realtorid']
        condition += "realtorid = %(realtorid)s AND "

    if "minprice" in filters:
        val = filters['minprice']
        query += "price >= %(minprice)s AND "
    if "maxprice" in filters:
        val = filters['maxprice']
        condition += "price < %(maxprice)s AND "
        
    if "minarea" in filters:
        val = filters['minarea']
        condition += "floorArea >= %(minarea)s AND "
    if "maxarea" in filters:
        val = filters['maxarea']
        condition += "floorArea < %(maxarea)s AND "

    if "minrooms" in filters:
        val = filters['minrooms']
        condition += "roomsNum >= %(minrooms)s AND "
    if "maxrooms" in filters:
        val = filters['maxrooms']
        condition += "roomsNum < %(maxrooms)s AND "

    if "name" in filters:
        val = filters['name']
        condition += "name LIKE %(name)s AND "
        filters['name'] = '%'+val+'%'
        
    if "description" in filters:
        val = filters['description']
        condition += "description LIKE %(description)s AND "
        filters['description'] = '%'+val+'%'

    condition_for_union = condition
    if "status" in filters:
        val = filters['status']
        condition += "status = %(status)s AND "

    condition = condition.strip()
    if condition.endswith('AND'):
        condition = condition[:-3]
        
    if condition.endswith('WHERE'):
        condition += ' 1'

    condition_for_union = condition_for_union.strip()
    if condition_for_union.endswith('AND'):
        condition_for_union = condition_for_union[:-3]
        
    if condition_for_union.endswith('WHERE'):
        condition_for_union += ' 1'
    if client_id is not None:
        query += " " + condition
        union_query=""" 
                        UNION
                        SELECT apartments.*
                        FROM apartments JOIN rentals on rentals.apartmentid = apartments.id
                    """
        query += " " + union_query
        if len(condition_for_union) > 0 and condition_for_union != ' 1':
            condition_for_union += " AND rentals.clientid={}".format(client_id)

        query += " " +condition_for_union
    else:
        query += " " + condition
                        

    return query, filters



def _deserialize_list(data, boxed_type):
    """Deserializes a list and its elements.

    :param data: list to deserialize.
    :type data: list
    :param boxed_type: class literal.

    :return: deserialized list.
    :rtype: list
    """
    return [_deserialize(sub_data, boxed_type)
            for sub_data in data]


def _deserialize_dict(data, boxed_type):
    """Deserializes a dict and its elements.

    :param data: dict to deserialize.
    :type data: dict
    :param boxed_type: class literal.

    :return: deserialized dict.
    :rtype: dict
    """
    return {k: _deserialize(v, boxed_type)
            for k, v in six.iteritems(data)}


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')




def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password

