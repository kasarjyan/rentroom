
import datetime
import json
import re
from dateutil.parser import parse

from flask_restful import reqparse, Resource
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flask import jsonify, Request
from flask import request
from apartments import app
from apartments.db import _db
from apartments.util import verify_password, hash_password
from apartments.encoder import box_identity
from apartments.encoder import unbox_identity

from apartments.models import User
from apartments.models import Apartment
from apartments.models import Rental
from apartments.models.permissions import Permissions, Permission
from apartments.encoder import CustomJSONEncoder


class RentalResource(Resource):
    """Get user with id userid"""
    @jwt_required
    def get(self, rentalid):
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        rental = Rental()
        rental.id = rentalid
        rental = _db.retrieve_rental(rental)
        if rental is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        apartment = Apartment()
        apartment.id = rental.apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if user.id != rental.clientid and user.id != apartment.realtorid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
        rental_json = json.loads(json.dumps(rental, cls=CustomJSONEncoder))
        result = jsonify(rental_json)
        return result

    """Update user with id userid"""
    @jwt_required
    def put(self, rentalid):
        parser = reqparse.RequestParser()
        parser.add_argument('startDate', type=str, help='First Name of user in JSON')
        parser.add_argument('endDate', type=str,  help='Last Name of user in JSON')
        args = parser.parse_args(strict=True)

        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result

        rental = Rental()
        rental.id = rentalid
        rental = _db.retrieve_rental(rental)
        if rental is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        apartment = Apartment()
        apartment.id = rental.apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if user.id != rental.clientid and user.id != apartment.realtorid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result


        modified_rental = Rental.from_dict(args)
        modified_rental.id = rentalid


        modified_rental = _db.update_rental(modified_rental)
        if modified_rental is None:
            result = jsonify(msg='Failed to update the rental')
            result.status_code = 403
            return result
        return  jsonify()



    """Delete user with id userid"""
    @jwt_required
    def delete(self, rentalid):
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 40
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result

        rental_to_delete = Rental()
        rental_to_delete.id = rentalid
        rental_to_delete = _db.retrieve_rental(rental_to_delete)
        if rental_to_delete is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        apartment = Apartment()
        apartment.id = rental_to_delete.apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if user.id != rental_to_delete.clientid and user.id != apartment.realtorid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result


        result = _db.delete_rental(rental_to_delete)
        if result is None:
            result = jsonify(msg='Failed to delete the rental')
            result.status_code = 400
            return result
        
        apartment.status = 'available'
        _db.update_apartment(apartment)
        return jsonify(msg='Rental deleted')



class RentalsResource(Resource):
    """description of class"""
    
    @jwt_required
    def post(self):
        """Create a user """
        parser = reqparse.RequestParser()

        parser.add_argument('apartmentid', type=str, required = True, help='Apartment id to rent')
        parser.add_argument('clientid', type=str, help='Renting client id. Required for Admins')
        parser.add_argument('startDate', type=str, required = True, help='Renting start date')
        parser.add_argument('endDate', type=str, required = True, help='Renting end date')
        args = parser.parse_args(strict=True)

        args['id'] = -1

        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        if not user.is_admin and not user.is_client:
            result = jsonify(msg='Permission denied')
            result.status_code = 403
            return result
        stDate = parse(args['startDate'])
        rental = Rental.from_dict(args)
        if rental.clientid != user.id :
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
            else:
                #Check for user to exist
                u = User()
                u.id = rental.clientid
                u = _db.retrieve_user_with_id(u)
                if u is None or not u.is_client:
                    result = jsonify(msg='Client not found')
                    result.status_codes = 400
                    return result
        apartment = Apartment()
        apartment.id = rental.apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if apartment is None:
            result = jsonify(msg='Apartment not found')
            result.status_codes = 400
            return result
        if apartment.status != 'available':
            result = jsonify(msg='Apartment is not avaiable')
            #precondition fail
            result.status_codes = 412 
            return result
        rental.booking_date = datetime.date.today()
        rental = _db.create_rental(rental)
        if rental is None or rental.id is None:
            result = jsonify(msg='Failed to create the rental')
            #todo: chack error code
            result.status_code = 401
            return result
        apartment.status = 'rented'
        _db.update_apartment(apartment)
        #Remove token generation keep only for login"        
        result = jsonify(id=rental.id)
        result.status_code = 201
        return result


    @jwt_required
    def get(self):
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        rentals_json=None
        if user.is_admin:
            rentals = _db.retrieve_rentals()
            rentals_json = json.loads(json.dumps(rentals, cls=CustomJSONEncoder))
        elif user.is_client:
            rentals = _db.retrieve_rentals(clientid=user.id)
            rentals_json = json.loads(json.dumps(rentals, cls=CustomJSONEncoder))
        elif user.is_realtor:
            rentals = _db.retrieve_rentals(realtorid=user.id)
            rentals_json = json.loads(json.dumps(rentals, cls=CustomJSONEncoder))

        result = jsonify(rentals=rentals_json)
        return result

    @jwt_required
    def delete(self):
        """Delete rental by  clientid and apartmentid"""
        parser = reqparse.RequestParser()
        parser.add_argument('apartmentid', type=str, required = True, help='Apartment id to rent')
        parser.add_argument('clientid', type=str, required = True, help='Rent client id')
        args = parser.parse_args(strict=True)
        
        args['id'] = -1
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        
        rental_to_delete = _db.retrieve_rental(args['apartmentid'], args['clientid'])
        if rental_to_delete is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        apartment = Apartment()
        apartment.id = rental_to_delete.apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if user.id != rental_to_delete.clientid and user.id != apartment.realtorid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
        result = _db.delete_rental(rental_to_delete)
        apartment.status = 'available'
        _db.update_apartment(apartment)
        if result is None:
            result = jsonify(msg='Failed to delete the rental')
            result.status_code = 400
            return result
        
        return jsonify(msg='Rental deleted')