import json
import math

from datetime import date, datetime, time
from flask_restful import reqparse, Resource
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flask import jsonify, Request
from flask import request
from apartments import app
from apartments.db import _db
from apartments.util import verify_password, hash_password
from apartments.encoder import unbox_identity
from apartments.models.user import User
from apartments.models.permissions import Permission
from apartments.models.permissions import Permissions
from apartments.models.apartment import  Apartment
from apartments.encoder import CustomJSONEncoder


class ApartmentResource(Resource):

    @jwt_required
    def get(self, apartmentid):
        """Get apartment with id apartmentid"""
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        apartment = Apartment()
        apartment.id = apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if apartment is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        if apartment.realtorid != user.id:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result

        apartment_json = json.loads(json.dumps(apartment, cls=CustomJSONEncoder))
        result = jsonify(apartment_json)
        return result


    @jwt_required
    def put(self, apartmentid):
        """Update apartment with id apartmentid"""
        parser = reqparse.RequestParser()
        parser.add_argument('realtorid', type=int, help='Realtor id(needed for admins)')
        parser.add_argument('name', type=str, help='Apartment name')
        parser.add_argument('description', type=str, help='Apartment description')
        parser.add_argument('floorArea', type=float, help='Apartment floor area size')
        parser.add_argument('price', type=float, help='Apartment rent price')
        parser.add_argument('roomsNum', type=int, help='Apartment rent price')
        parser.add_argument('latitude', type=float, help='Apartment Geoposition latitude')
        parser.add_argument('longitude', type=float, help='Geoposition longitude')
        args = parser.parse_args(strict=True)


        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result

        apartment = Apartment()
        apartment.id = apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if apartment is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        if apartment.realtorid != user.id:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
                
        modified_apartment = Apartment.from_dict(args)
        if modified_apartment.realtorid != None and modified_apartment.realtorid != apartment.realtorid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result

        modified_apartment.id = apartmentid

        result = _db.update_apartment(modified_apartment)
        if result is None:
             result = jsonify(msg='Failed to update the apartment')
             result.status_code = 403
             return result
        return  jsonify()

    @jwt_required
    def delete(self, apartmentid):
        """Delete apartment with id apartmentid"""
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        apartment = Apartment()
        apartment.id = apartmentid
        apartment = _db.retrieve_apartment(apartment)
        if apartment is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        if apartment.realtorid != user.id:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
        result = _db.delete_apartment(apartment)
        if result is None:
             result = jsonify(msg='Failed to delete the apartment')
             #todo: chack error code
             result.status_code = 400
             return result

        result = jsonify(msg='Apartment deleted')
        result.status_code = 204
        return result


class ApartmentsResource(Resource):

    """description of class"""
    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('realtorid', type=int, help='Realtor id(needed for admins)')
        parser.add_argument('name', type=str, required = True, help='Apartment name')
        parser.add_argument('description', type=str, required = True, help='Apartment description')
        parser.add_argument('floorArea', type=float, required = True, help='Apartment floor area size')
        parser.add_argument('price', type=float, required = True, help='Apartment rent price')
        parser.add_argument('roomsNum', type=int, required = True, help='Apartment rent price')
        parser.add_argument('latitude', type=float, required = True, help='Apartment Geoposition latitude')
        parser.add_argument('longitude', type=float, required = True, help='Geoposition longitude')

        
        args = parser.parse_args(strict=True)
        args['id'] = -1
        if args['realtorid'] is None:
            del args['realtorid']

        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result

        apartment = Apartment.from_dict(args)
        if apartment.realtorid is not None and apartment.realtorid != user.id :
            #Apartment different realtor id is provided, check for permission
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
            else:
                #Check for user to exist
                u = User()
                u.id = apartment.realtorid
                u = _db.retrieve_user_with_id(u)
                if u is None or not u.is_realtor:
                    result = jsonify(msg='Realtor not found')
                    result.status_codes = 400
                    return result

        if apartment.realtorid is None:
            if user.is_realtor:
                apartment.realtorid = user.id
            else:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result

        if apartment.realtorid == user.id:
            if not user.is_realtor:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result

        apartment.date_added = date.today()
        apartment.status = 'available'
        apartment = _db.create_apartment(apartment)
        
        if apartment is None:
             result = jsonify(msg='Failed to create the apartment')
             #todo: chack error code
             result.status_code = 401
             return result
        #Remove token generation keep only for login"
        result = jsonify(id=apartment.id, realtorid=apartment.realtorid)
        result.status_code = 201
        return result

    @jwt_required
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("realtorid", type=int, help='realtor id')
        parser.add_argument('minprice', type=float,  help='minimum rent price of apartment')
        parser.add_argument('maxprice', type=float,  help='maximum rent price of apartment')
        parser.add_argument('minarea', type=float, help='minimum area of apartment')
        parser.add_argument('maxarea', type=float,  help='maximum area of apartment')
        parser.add_argument('minrooms', type=int, help='minimum rooms of apartment')
        parser.add_argument('maxrooms', type=int, help='maximum rooms of apartment')
        parser.add_argument('name', type=str, help='name of apartment')
        parser.add_argument('description', type=str, help='description of apartment')
        args = parser.parse_args(strict=True) 
        
        identity = get_jwt_identity()

        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        apartment = None

        filters = dict([(a,b) for a, b in args.items() if b != None])

        if 'realtorid' in filters:
            if filters['realtorid'] != user.id:
                if user.is_admin:
                    #Check for user to exist
                    u = User()
                    u.id = filters['realtorid']
                    u = _db.retrieve_user_with_id(u)
                    if u is None or not u.is_realtor:
                        result = jsonify(msg='Realtor not found')
                        result.status_codes = 400
                        return result
                    apartments = _db.retrieve_apartments(filters)
                else:
                    #Realtor id is provided which dont match requester id and requester not an admin
                    result = jsonify(msg='Permission denied')
                    result.status_code = 403
                    return result
            else:
                if user.is_realtor:
                    apartments = _db.retrieve_apartments(filters)
                else:
                    result = jsonify(msg='Realtor not found')
                    result.status_codes = 400
                    return result
        else:
            #Realtor is not provided
            if user.is_realtor:
                filters['realtorid'] = user.id
                apartments = _db.retrieve_apartments(filters)
            elif user.is_admin:
                apartments = _db.retrieve_apartments(filters)
            elif user.is_client:
                filters['status'] = 'available'
                apartments = _db.retrieve_apartments(filters, user.id)
            else:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result

        apartments_json = json.loads(json.dumps(apartments, cls=CustomJSONEncoder))

        result = jsonify(apartments=apartments_json)
        return result