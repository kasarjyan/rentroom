import datetime
import json
import re

from flask_restful import reqparse, Resource
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flask import jsonify, Request
from flask import request
from apartments import app
from apartments.db import _db
from apartments.util import verify_password, hash_password
from apartments.encoder import box_identity
from apartments.encoder import unbox_identity

from apartments.models.user import User
from apartments.models.permissions import Permissions, Permission
from apartments.encoder import CustomJSONEncoder


class ClientResource(Resource):
    """Get user with id clientid"""
    @jwt_required
    def get(self, clientid):
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        if user.id != clientid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
        u = User()
        u.id = clientid
        user = _db.retrieve_user_with_id(u)
        if user is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        user_json = json.loads(json.dumps(user, cls=CustomJSONEncoder))
        del user_json['password']
        result = jsonify(user_json)
        return result

    """Update user with id clientid"""
    @jwt_required
    def put(self, clientid):

        parser = reqparse.RequestParser()
        parser.add_argument('firstName', type=str, help='First Name of user in JSON')
        parser.add_argument('lastName', type=str,  help='Last Name of user in JSON')        
        parser.add_argument('password', type=str,  help='Password Sha-256 in JSON')
        args = parser.parse_args(strict=True)

        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        if user.id != clientid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
        modified_user = User.from_dict(args)
        modified_user.id = clientid
        
        if modified_user.password is not None:
            modified_user.password = hash_password(modified_user.password)

        modified_user = _db.update_user(modified_user)
        if modified_user is None:
            result = jsonify(msg='Failed to update the user')
            result.status_code = 403
            return result
        return  jsonify()



    """Delete user with id clientid"""
    @jwt_required
    def delete(self, clientid):
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 40
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        if user.id != clientid:
            if not user.is_admin:
                result = jsonify(msg='Permission denied')
                result.status_code = 403
                return result
        user_to_delete = User()
        user_to_delete.id = clientid
        user_to_delete = _db.retrieve_user_with_id(user_to_delete)
        if user_to_delete is None:
            result = jsonify(msg='Resource not found')
            result.status_code = 404
            return result
        result = _db.delete_user(user_to_delete)
        if result is None:
            result = jsonify(msg='Failed to delete the user')
            result.status_code = 400
            return result
        
        return jsonify(msg='User deleted')



class ClientsResource(Resource):
    """description of class"""
    def post(self):
        """Create a user """
        parser = reqparse.RequestParser()

        parser.add_argument('firstName', type=str, required = True, help='First Name of user in JSON')
        parser.add_argument('lastName', type=str, required = True, help='Last Name of user in JSON')
        parser.add_argument('email', type=str, required = True, help='E-Mail of user in JSON')
        parser.add_argument('password', type=str, required = True, help='Password Sha-256 in JSON')

        args = parser.parse_args(strict=True)        
        args['id'] = -1
        user = User.from_dict(args)
        prog = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        #check whether the email is valid
        if prog.match(user.email) is None:
            result = jsonify(msg='Bad email address syntax')
            result.status_code = 513
            return result
        retrieved_user = _db.retrieve_user(user)
        if retrieved_user is not None:
            result = jsonify(msg='User alredy exists')
            result.status_code = 409
            return result
        
        user.role = 3
        user.password = hash_password(user.password)
        user = _db.create_user(user)
        
        if user is None:
            result = jsonify(msg='Failed to create user')
            result.status_code = 401
            return result
        expire_delta = datetime.timedelta(float(1.0))
        #identity = box_identity(user,_db.retrieve_user_permissions(user))
        #access_token = create_access_token(identity=identity, expires_delta=expire_delta)
        result = jsonify(msg='User created')
        result.status_code = 201
        return result

    @jwt_required
    def get(self):
        identity = get_jwt_identity()
        if identity is None:
            result = jsonify(msg='Authentication failed')
            result.status_code = 401
            return result
        user, permissions = unbox_identity(identity)
        if user is None or permissions is None:
            result = jsonify(msg='Token is damaged')
            result.status_code = 401
            return result
        if not user.is_admin:
            result = jsonify(msg='Permission denied')
            result.status_code = 403
            return result
        
        users = _db.retrieve_users(3)
        users_json = json.loads(json.dumps(users, cls=CustomJSONEncoder))        
        for u in users_json:
            del u['password']
            del u['role']

        result = jsonify(clients=users_json)
        return result