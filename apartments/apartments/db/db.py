
import mysql.connector

from apartments.models import User
from apartments.models import Apartment
from apartments.models import Rental
from apartments.models.permissions import Permissions
from apartments import app
from apartments.util import generate_insert_query, generate_update_query, generate_select_apartments_query

class db:
    DB_HOST = app.config['DB_HOST']
    DB_USERNAME = app.config['DB_USERNAME']
    DB_PASSWORD = app.config['DB_PASSWORD']
    DB_PORT = app.config['DB_PORT']
    DB_NAME = app.config['DB_NAME']

    def create_connection(self):
        connection = mysql.connector.connect(host=self.DB_HOST,
            user=self.DB_USERNAME,
            password=self.DB_PASSWORD,
            port=self.DB_PORT,
            database=self.DB_NAME)
        return connection



    def create_user(self, user: User):
        connection = self.create_connection()
        cursor = connection.cursor()
        #retrieve before insert
        query, values_map = generate_insert_query(user, 'users')
        cursor.execute(query, values_map)        
        if cursor.rowcount > 0 :
            user.id = cursor.lastrowid
        
        connection.commit()
        connection.close()
        return user

    def retrieve_user(self, user: User):
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        cursor.execute("""SELECT id, firstName, lastName, email, password, role 
                          FROM users
                          WHERE email=%(email)s""", {'email':user.email})
        rows = cursor.fetchall()
        if len(rows) == 1:
            user = User.from_dict(rows[0])
            return user
        return None
    def retrieve_user_with_id(self, user: User):
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        cursor.execute("""SELECT id, firstName, lastName, email, password, role 
                          FROM users
                          WHERE id=%(id)s""", {'id':user.id})
        rows = cursor.fetchall()
        if len(rows) == 1:
            user = User.from_dict(rows[0])
            return user
        return None
    def retrieve_users(self, role):        
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        #Add pagination
        if role is None:
            cursor.execute("""SELECT * FROM users""")
        else:
            cursor.execute("""SELECT * FROM users WHERE role=%(role)s""", {"role":role})
        users=[]
        for row in cursor:
            users.append(User.from_dict(row))            
        connection.close()
        return users


    def update_user(self, user: User):
        connection = self.create_connection()
        cursor = connection.cursor()
        #retrieve before insert
        query, values_map = generate_update_query(user, 'users')
        cursor.execute(query, values_map)
        res = None
        if cursor.rowcount > 0 :
            res  = True
        connection.commit()
        connection.close()
        return res


    def delete_user(self, user: User):        
        connection = self.create_connection()
        cursor = connection.cursor()
        cursor.execute("""DELETE FROM users WHERE id=%(id)s""", {'id': user.id})
        result = None
        if cursor.rowcount > 0 :
            result = True
        connection.commit()
        connection.close()
        return result

    def create_apartment(self, apartment: Apartment):
        connection = self.create_connection()
        cursor = connection.cursor()
        #retrieve before insert
        query, values_map = generate_insert_query(apartment, 'apartments')
        cursor.execute(query, values_map)        
        if cursor.rowcount > 0 :
            apartment.id = cursor.lastrowid
        connection.commit()
        connection.close()
        return apartment
    

    def retrieve_apartment(self, apartment: Apartment):
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        cursor.execute("""SELECT * 
                          FROM apartments
                          WHERE id=%(id)s""", {'id':apartment.id})
        rows = cursor.fetchall()
        if len(rows) == 1:
            apartment = Apartment.from_dict(rows[0])
            return apartment
        return None

    def retrieve_apartments(self, filters, client_id=None):        
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        query, values = generate_select_apartments_query(filters, client_id)
        #Add pagination
        cursor.execute(query, values)
        apartments=[]
        for row in cursor:
            apartments.append(Apartment.from_dict(row))            
        connection.close()
        return apartments
#UNION
#SELECT apartments.*
#FROM apartments JOIN rentals on rentals.apartmentid = apartments.id

    def update_apartment(self, apartment: Apartment):
        connection = self.create_connection()
        cursor = connection.cursor()
        #retrieve before insert
        query, values_map = generate_update_query(apartment, 'apartments')
        cursor.execute(query, values_map)
        res = None
        if cursor.rowcount > 0 :
            res  = True
        connection.commit()
        connection.close()
        return res

    def delete_apartment(self, apartment: Apartment):        
        connection = self.create_connection()
        cursor = connection.cursor()
        cursor.execute("""DELETE FROM apartments WHERE id=%(id)s""", {'id': apartment.id})
        result = None
        if cursor.rowcount > 0 :
            result = True
        connection.commit()
        connection.close()
        return result
    ##Custom methods should be checked
    #def retrieve_user_expenses(self, user: User, filters):
    #    connection = self.create_connection()
    #    cursor = connection.cursor(dictionary=True)
    #    query, values = generate_select_expenses_query(filters)

    #    #Add pagination
    #    cursor.execute(query, values)
    #    expenses=[]
    #    for row in cursor:
    #        expenses.append(Expense.from_dict(row))            
    #    connection.close()
    #    return expenses
    def create_rental(self, rental: Rental):
        connection = self.create_connection()
        cursor = connection.cursor()
        #retrieve before insert
        query, values_map = generate_insert_query(rental, 'rentals')
        cursor.execute(query, values_map)        
        if cursor.rowcount > 0 :
            rental.id = cursor.lastrowid
        connection.commit()
        connection.close()
        return rental

    def update_rental(self, rental: Rental):
        connection = self.create_connection()
        cursor = connection.cursor()
        #retrieve before insert
        query, values_map = generate_update_query(rental, 'rentals')
        cursor.execute(query, values_map)
        res = None
        if cursor.rowcount > 0 :
            res  = True
        connection.commit()
        connection.close()
        return res

    def delete_rental(self, rental: Rental):        
        connection = self.create_connection()
        cursor = connection.cursor()
        cursor.execute("""DELETE FROM rentals WHERE id=%(id)s""", {'id': rental.id})
        result = None
        if cursor.rowcount > 0 :
            result = True
        connection.commit()
        connection.close()
        return result    

    def retrieve_rental(self, rental: Rental):
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        cursor.execute("""SELECT * 
                          FROM rentals
                          WHERE id=%(id)s""", {'id':rental.id})
        rows = cursor.fetchall()
        if len(rows) == 1:
            rental = Rental.from_dict(rows[0])
            return rental
        return None

    def retrieve_rental(self, apartmentid, clientid):
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        cursor.execute("""SELECT *
                          FROM rentals
                          WHERE clientid=%(clientid)s and apartmentid=%(apartmentid)s""", {'clientid':clientid, 'apartmentid':apartmentid})
        rows = cursor.fetchall()
        if len(rows) > 0:
            rental = Rental.from_dict(rows[0])
            return rental
        return None

    def retrieve_rentals(self, clientid=None, realtorid=None):
        connection = self.create_connection()
        cursor = connection.cursor(dictionary=True)
        query = "SELECT rentals.id as id, rentals.apartmentid as apartmentid, rentals.clientid as clientid, rentals.bookingDate as bookingDate, rentals.startDate as startDate, rentals.endDate as endDate FROM rentals "
        if realtorid is not None:
            query += " JOIN apartments ON rentals.apartmentid = apartments.id WHERE realtorid={} ".format(realtorid)
        if clientid is not None and realtorid is None:
            query += " WHERE rentals.clientid = {}".format(clientid)
        elif clientid is not None and realtorid is not None:
            query += " and rentals.clientid = {}".format(clientid)

        #query, values = generate_select_apartments_query(filters)
        #Add pagination
        cursor.execute(query)
        rentals=[]
        for row in cursor:
            rentals.append(Rental.from_dict(row))            
        connection.close()
        return rentals


    def retrieve_user_permissions(self, user: User):
        connection = self.create_connection()
        cursor = connection.cursor()
        cursor.execute("""SELECT permissions.name 
                          FROM roles JOIN role_permissions ON roles.id = role_permissions.role_id JOIN permissions ON role_permissions.permission_id = permissions.id
                          WHERE roles.id=%(role_id)s""", {'role_id': user.role})
        permission_list=[]
        for row in cursor:
            permission_list.append(row[0])

        permissions = Permissions(permission_list)
        return permissions



    