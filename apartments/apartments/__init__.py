"""
The flask application package.
"""

from flask import Flask
from flask import jsonify
from flask_jwt_extended import JWTManager
from flask_jwt_extended.exceptions import *
from flask_restful import Api, Resource
from flask_jwt_extended.config import config
app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'super-secret-key-hovhannes-kasarjyan'
app.config['DB_HOST'] = 'localhost'
app.config['DB_USERNAME']='hovhannes'
app.config['DB_PASSWORD']='kasarjyan'
app.config['DB_PORT']=3306
app.config['DB_NAME']='apartmentsdb'

jwt = JWTManager(app)
class CustomApi(Api):
    def handle_error(self, e):
        for val in current_app.error_handler_spec.values():
            for handler in val.values():
                registered_error_handlers = list(filter(lambda x: isinstance(e, x), handler.keys()))
                if len(registered_error_handlers) > 0:
                    raise e
        return super().handle_error(e)


api = CustomApi(app)

version = 'v1.0'
prefix = '/api/{}'.format(version)


import flask_jwt_extended.default_callbacks



from  apartments.resources.login import Login
from  apartments.resources.clients import ClientResource, ClientsResource
from  apartments.resources.realtors import RealtorResource, RealtorsResource
from  apartments.resources.admins import AdminResource, AdminsResource
from  apartments.resources.apartments import ApartmentResource, ApartmentsResource
from  apartments.resources.rentals import RentalResource, RentalsResource

api.add_resource(Login, '/login')

api.add_resource(ClientResource,'/clients/<int:clientid>')
api.add_resource(ClientsResource, '/clients')

api.add_resource(AdminResource,'/admins/<int:userid>')
api.add_resource(AdminsResource, '/admins')

api.add_resource(RealtorResource,'/realtors/<int:userid>')
api.add_resource(RealtorsResource, '/realtors')

api.add_resource(ApartmentResource,'/apartments/<int:apartmentid>')
api.add_resource(ApartmentsResource, '/apartments')


api.add_resource(RentalResource,'/rentals/<int:rentalid>')
api.add_resource(RentalsResource, '/rentals')
